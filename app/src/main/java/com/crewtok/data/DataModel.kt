package com.crewtok.data

import android.os.Parcel
import android.os.Parcelable

/*{"Email":"umesh.kumar@appworldcenter.com",
    "image":"~/assets/images/icon/staff.png","name":"umesh kamboj","firstname":"umesh",
    "lastname":"kamboj"}*/
data class UserModel(
    val Email: String,
    val image: String,
    val name: String,
    val firstname: String,
    val lastname: String
)

/*{
			"jobId": 45,
			"jobName": "testjob1",
			"image": "http://maps.google.com/mapfiles/ms/icons/blue.png",
			"po": "87",
			"phone": "9813151230",
			"assignby": "umesh kamboj",
			"assigndate": "1/11/2019 10:15:00 AM",
			"customername": "umesh",
			"location": "6321251, 555 Washington Ave, city, stATE, 135133",
			"status": "New",
			"statuscolor": "#398BF7",
			"datecomplete": "5/7/2019 9:20:31 AM",
			"token": "EAAAAFqyN3Crggko1YDSNGBoRkpqHTht9Acv3rnznICgmwAZ"
		}*/

data class AssignmentModel(
    val jobId: Int, val jobName: String, val image: String, val po: String, val phone: String,
    val assignby: String, val assigndate: String, val customername: String, val location: String,
    val status: String, val statuscolor: String, val datecomplete: String, val token: String, var checked: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun equals(other: Any?): Boolean {
        return super.equals(other) && this.jobId == (other as AssignmentModel).jobId
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(jobId)
        parcel.writeString(jobName)
        parcel.writeString(image)
        parcel.writeString(po)
        parcel.writeString(phone)
        parcel.writeString(assignby)
        parcel.writeString(assigndate)
        parcel.writeString(customername)
        parcel.writeString(location)
        parcel.writeString(status)
        parcel.writeString(statuscolor)
        parcel.writeString(datecomplete)
        parcel.writeString(token)
        parcel.writeByte(if (checked) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AssignmentModel> {
        override fun createFromParcel(parcel: Parcel): AssignmentModel {
            return AssignmentModel(parcel)
        }

        override fun newArray(size: Int): Array<AssignmentModel?> {
            return arrayOfNulls(size)
        }
    }
}

/*
* {
		"date": "05/23/2019",
		"time": "02:10 PM",
		"jobid": 4,
		"JobName": "app test3",
		"Location": "111 East 1st Street #\u003cbr/\u003eAustin, Texas 78701",
		"description": "\u003cp\u003eDescript\u003c/p\u003e\r\n\u003cp\u003eion\u003c/p\u003e",
		"statuscolor": "#ffb22b",
		"status": "In progress",
		"statusid": 2,
		"equipments": [{
			"name": "Equipment1",
			"id": "4",
			"unit": "2"
		}],
		"inventory": [{
			"name": "app2",
			"id": "3",
			"unit": "2"
		}],
		"latestComment": "test",
		"attachments": []
	}
* */

data class AssignmentDetailModel(
    val jobid: Int, val date: String, val time: String, val JobName: String, val Location: String,
    val description: String?, val statuscolor: String?, val status: String, val statusid: Int,
    val inventory: ArrayList<InventoryModel>, val equipments: ArrayList<EquipmentModel>, val latestComment: String?,
    val latestCommentimage: String?,
    val attachments: ArrayList<AttachmentModel>
)

data class InventoryModel(val id: String, val name: String, val unit: String)

data class EquipmentModel(
    val id: String,
    val name: String,
    val quantity: String,
    val date: String,
    val time: String,
    val serialnumber: String,
    val status: String,
    val assignedby: String
)

data class AttachmentModel(val url: String, val name: String, var type: String = "image")

data class ContactModel(
    val Company: Any?,
    val GroupId: Int,
    val ManagerId: Int,
    val _address: Any?,
    val _equipmets: Any?,
    val _supply: Any?,
    val companyId: Int,
    val companyname: Any?,
    val createdDate: String,
    val description: Any?,
    val discount: Any?,
    val email: String,
    val fname: Any?,
    val id: Int,
    val image: String,
    val lname: Any?,
    val name: String,
    val permissionId: Int,
    val pgroup: String,
    val phone: String,
    val position: Any?,
    val skills: Any?,
    val status: Boolean,
    val token: String
)

data class InventoryCategoryModel(val tagid: Int, val Tagname: String, val description: String?)
data class InventoryItemModel(val id: Int, val name: String, val description: String?, val Quantity:Int)
data class ItemCommentModel(
    val id: Int, val name: String, val body: String?, val image: String?,
    val commentimage: String?, val craeteddate: String?
)

data class ItemContactGroupModel(val id: Int, val name: String, var checked: Boolean = false)


/*{
			"Image": "/assets/images/icon/staff.png",
			"message": "\u003cp\u003ehello\u003c/p\u003e\n\n\u003cp\u003e \u003c/p\u003e\n",
			"messageId": 3,
			"Name": "Dev One",
			"sender": 1,
			"Time": "11:26 AM",
			"userId": 1
		}*/

data class ItemChatModel(
    val Image: String?,
    val message: String?,
    val Time: String?,
    val sender: String,
    val messageId: Int
){

    override fun equals(other: Any?): Boolean {
        return messageId==(other as ItemChatModel).messageId
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}