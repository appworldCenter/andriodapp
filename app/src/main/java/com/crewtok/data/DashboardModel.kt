package com.crewtok.data

data class DashboardModel(
    val assignjobs: Int = 0,
    val chatcontactid: Int = 0,
    val equipments: Int = 0,
    val hour: String = "",
    val profileimage: String = "",
    val latest_message: String = "",
    val unreadchat: String = ""
)