package com.crewtok.network;


import android.app.ProgressDialog;
import android.os.Build;
import android.text.TextUtils;

import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.crewtok.BuildConfig;
import com.crewtok.core.MyApplication;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author rahul
 */
public class ApiManager implements JSONObjectRequestListener {
    public final static String DEV_PROJECT_ROOT_URL = "https://dev.crewtok.com/";
        public final static String LIVE_PROJECT_ROOT_URL = "https://app.crewtok.com/";

    public final static String BASE_URL = DEV_PROJECT_ROOT_URL + "json/";


    private static ApiManager apiManager;
    private ApiResponse apiResponse;
    private ApiMode mode;
    private ApiMode cancelledApiMode;
    private static ProgressDialog progressDialog;
    private static JsonParser jsonParser;
    private boolean showLoader = false;

    public static void init() {
        apiManager = new ApiManager();
        jsonParser = new JsonParser();
    }

//    private static void initProgress(Activity context) {
//        progressDialog = new ProgressDialog(context);
//
//        progressDialog.show();
//    }

    public static ApiManager getInstance() {
        if (apiManager == null) {
            init();
        }
        return apiManager;
    }

    @Override
    public void onResponse(JSONObject response) {


        dismissDialog();
        try {
            if (response.isNull("error") || TextUtils.isEmpty(response.getString("error"))) {
                apiResponse.onSuccess(getSerializedJson(response.toString()), mode);
            } else {
                apiResponse.onFailure(getSerializedJson(response.toString()), mode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.onException(e, mode);
        }
    }

    private void dismissDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception ex) {

        }
    }

    @Override
    public void onError(ANError anError) {
        dismissDialog();
        if (anError.getErrorCode() != 0) {
            apiResponse.onFailure(getSerializedJson(anError.getErrorBody()), mode);
        } else {
            apiResponse.onException(anError, mode);
        }
    }

    private JsonObject getSerializedJson(String jsonObject) {
        //        Log.e("API_RESPONSE", jsonObject);
        return jsonParser.parse(jsonObject).getAsJsonObject();
    }

    public synchronized void requestApi(ApiMode mode,
                                        HashMap<String, Object> requestBody,
                                        boolean showLoader, ApiResponse response,
                                        String requestType) {
        MyApplication.instance.getContext();
        /*          AppUtils.hideKeyboard();*/
        this.apiResponse = response;
        this.mode = mode;
        this.showLoader = showLoader;
        if (showLoader && !MyApplication.instance.getContext().isFinishing()) {
            progressDialog = new ProgressDialog(MyApplication.instance.getContext());
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Processing...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
        switch (mode) {
            default:
                startApi(requestBody, mode.getName(), requestType);
                break;
        }
    }

    private void startApi(HashMap<String, Object> requestBody, String path,
                          String requestType) {

//        AndroidNetworking.cancel(mode.getName());

        switch (requestType) {
            case "post":
            case "POST":
                postRequestBody(requestBody, path).getAsJSONObject(this);
                break;
            case "get":
            case "GET":
                getRequestBody(requestBody, path).getAsJSONObject(this);
                break;
            default:
                break;
        }
    }

    private ANRequest getRequestBody(HashMap<String, Object> params, String path) {
        ANRequest.GetRequestBuilder builder = new ANRequest.GetRequestBuilder(BASE_URL + path);

        /*common param for all api*/
        if (params == null) {
            params = new HashMap<>();
        }

        if (params.size() > 0) {
            Set paramsSet = params.entrySet();
            for (Object aParamsSet : paramsSet) {
                Map.Entry pair = (Map.Entry) aParamsSet;
                builder.addQueryParameter(String.valueOf(pair.getKey()),
                        String.valueOf(pair.getValue()));
            }
        }
        return builder.build();
    }

    private ANRequest postRequestBody(HashMap<String, Object> params, String path) {
        ANRequest.PostRequestBuilder builder = new ANRequest.PostRequestBuilder(BASE_URL + path);

        /*common param for all api*/
        if (params == null) {
            params = new HashMap<>();
        }

        if (params.size() > 0) {
            Set paramsSet = params.entrySet();
            for (Object aParamsSet : paramsSet) {
                Map.Entry pair = (Map.Entry) aParamsSet;
                builder.addQueryParameter(String.valueOf(pair.getKey()),
                        String.valueOf(pair.getValue()));
            }
        }
        return builder.build();
    }


    public void cancelApi(ApiMode mode) {
        cancelledApiMode = mode;
//        AndroidNetworking.cancel(mode.getName());
    }

}
