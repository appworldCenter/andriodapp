package com.crewtok.network;

/**
 * @author rahul
 */

public enum ApiMode {
    LOGIN("login"),
    CLOCKIN("getclockinout"),
    UPDATE_CLOCKIN_STATUS("updateclockinout"),
    ASSIGNMENT_LIST("GetassignedJobs"),
    CONTACT_LIST("Getcontacts"),
    CONTACT_GROUP_LIST("Getcontactgroup"),
    ADD_COMMENT("addcomment"),
    ADD_ATTACHMENT("SavedocByjob"),
    UPDATE_JOB_STATUS("updatestatus"),
    COMMENT_LIST("viewcomment"),
    DASHBOARD("getdashboard"),
    EQUIPMENT_LIST("Getassignedequip"),
    JOB_DESCRIPTION("jobdescription"),
    JOB_CONTACT("jobcontact"),
    INVENTORY_CATEGORY("getinventoryCategory"),
    INVENTORY_ITEMS("getinventorybyCategory"),
    ADD_INVENTORY("addinventory"),
    CHAT_HISTORY("getchatbycontact"),
    ADD_CHAT("AddChat"),
    AJAX_CHAT("getchatbyajax"),
    UPDATE_LIVE_LOCATION("updatelivestatus"),
    ASSIGNMENT_DETAIL("getjob");


    private String action;

    ApiMode(String name) {
        this.action = name;
    }

    public String getName() {
        return this.action;
    }


}
