package com.crewtok.util

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.crewtok.core.MyApplication
import com.crewtok.network.ApiManager
import java.text.SimpleDateFormat
import java.util.*


class AppUtils {

    companion object {
        fun openGmail(subject: String, receiverEmail: String) {
            val emailIntent = Intent(Intent.ACTION_VIEW)

            emailIntent.data = Uri.parse("mailto:")

            emailIntent.putExtra(
                android.content.Intent.EXTRA_EMAIL,
                arrayOf(receiverEmail)
            )
            emailIntent.putExtra(
                android.content.Intent.EXTRA_SUBJECT,
                subject
            )
            try {
                startActivity(MyApplication.instance.getContext(), emailIntent, null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun openDialer(phone: String) {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$phone")
            startActivity(MyApplication.instance.getContext(), intent, null)
        }

        @JvmStatic
        fun shortToast(msg: String?) {
            Toast.makeText(MyApplication.instance.getContext(), msg, Toast.LENGTH_SHORT).show()
        }

        @SuppressLint("HardwareIds")
        fun getDeviceId(): String {
            return Settings.Secure.getString(
                MyApplication.instance.getContext().contentResolver,
                Settings.Secure.ANDROID_ID
            )
        }

        fun showException() {
            Toast.makeText(
                MyApplication.instance.getContext(),
                "Something went wrong. Please try again!",
                Toast.LENGTH_SHORT
            ).show()
        }

        fun getCurrentDate(): String {
            return SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).format(Date())
        }

        fun getCurrentTime(): String {
            return SimpleDateFormat("hh:mm a", Locale.getDefault()).format(Date())
        }

        fun getFullImageUrl(image: String): String {
            var startPos = 1
            if (image.contains("~")) startPos = 1
            return ApiManager.DEV_PROJECT_ROOT_URL + image.substring(startPos, image.length)
        }

        fun openBrowser(url: String) {
            val i = Intent(Intent.ACTION_VIEW)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            i.data = Uri.parse(url)
            MyApplication.instance.getContext().startActivity(i)
        }

        fun isImageUrl(url: String): Boolean {
            val extension = url.substring(url.lastIndexOf(".") + 1)
            return (extension == "png" || extension == "jpg" || extension == "jpeg")
        }
    }
}