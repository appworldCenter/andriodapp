package com.crewtok.util

import androidx.core.content.FileProvider


class GenericFileProvider : FileProvider() {
    companion object {
        @JvmField
        var AUTHORITY: String = "com.crewtok.fileprovider"
    }
}