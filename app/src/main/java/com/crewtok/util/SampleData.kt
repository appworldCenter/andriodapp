package com.crewtok.util

import androidx.annotation.DrawableRes
import com.crewtok.R

class SampleData {


    companion object {


        fun getHomeOptionList(): ArrayList<HomeOption> {

            val list = arrayListOf<HomeOption>()
            list.add(
                HomeOption(
                    "Time clock",
                    "",
                    R.drawable.time_clock
                )
            )
            list.add(
                HomeOption(
                    "Jobs",
                    "",
                    R.drawable.worker
                )
            )
            list.add(
                HomeOption(
                    "Communicate",
                    "",
                    R.drawable.chatting
                )
            )
            list.add(
                HomeOption(
                    "Equipment",
                    "",
                    R.drawable.equipment
                )
            )

            return list
        }


    }
}


data class HomeOption(val title: String, var subtitle: String, @DrawableRes val image: Int)