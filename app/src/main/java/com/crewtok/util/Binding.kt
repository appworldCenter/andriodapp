package com.crewtok.util

import android.view.View
import androidx.databinding.BindingAdapter
import com.crewtok.base.BaseActivity


@BindingAdapter("bind:back_nav")
fun bindBackNav(imageView: View, finish: Boolean) {

    imageView.setOnClickListener {
        if (finish) {
            (it.context as BaseActivity).finish()
        } else {
            (it.context as BaseActivity).onBackPressed()
        }
    }
}