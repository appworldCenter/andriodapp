package com.crewtok.util;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Looper;


import androidx.core.app.ActivityCompat;
import com.crewtok.core.MyApplication;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

/**
 * @author rahul
 */
public class TbmCurrentLocation {


    public void fetchLocation(final CurrentLocationCallback callback) {

        LocationManager manager = (LocationManager) MyApplication.instance.getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AppUtils.shortToast("GPS is not enabled. Please enable GPS first");
            return;
        }

        final ProgressDialog dialog = new ProgressDialog(MyApplication.instance.getContext());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage("Fetching location...");

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                dialog.dismiss();
                if (locationResult != null) {
                    Location location = locationResult.getLastLocation();
                    if (location != null) {
                        callback.currentLocationFetched(location.getLatitude(), location.getLongitude());
                    } else {
                        callback.currentLocationFetchFailed("Failed to get current location");
                    }
                } else {
                    callback.currentLocationFetchFailed("Failed to get current location");
                }
            }
        };
        if (ActivityCompat.checkSelfPermission(MyApplication.instance.getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MyApplication.instance.getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MyApplication.instance.getContext(), AppConstants.LOCATION_PERMISSIONS,
                    AppConstants.LOCATION_PERMISSIONS_CODE);
            return;
        }

        getFusedLocationProviderClient(MyApplication.instance.getContext())
                .requestLocationUpdates(mLocationRequest,
                        locationCallback,
                        Looper.myLooper());
    }

    public interface CurrentLocationCallback {
        void currentLocationFetched(double lat, double lng);

        void currentLocationFetchFailed(String error);
    }
}
