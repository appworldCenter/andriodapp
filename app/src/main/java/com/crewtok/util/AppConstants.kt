package com.crewtok.util

object AppConstants {

    @JvmField
    val LOCATION_PERMISSIONS =
        arrayOf("android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION")
    const val LOCATION_PERMISSIONS_CODE = 201

    val ALL_PERMISSIONS= arrayOf("android.permission.ACCESS_COARSE_LOCATION",
        "android.permission.ACCESS_FINE_LOCATION")
}