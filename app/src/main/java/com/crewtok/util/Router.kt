import android.app.Activity
import android.content.Intent
import androidx.annotation.Nullable
import com.crewtok.core.MyApplication

object Router {

    inline fun <reified T : Activity> start(newTask: Boolean) {
        val context = MyApplication.instance.getContext()
        val intent = Intent(context, T::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
        context.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    inline fun <reified T : Activity> start() {
        val context = MyApplication.instance.getContext()
        val intent = Intent(context, T::class.java)
        context.startActivity(intent)
        context.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

    }
}