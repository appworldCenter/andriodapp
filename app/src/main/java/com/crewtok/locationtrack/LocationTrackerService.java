package com.crewtok.locationtrack;

import android.Manifest;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.crewtok.network.ApiManager;
import com.crewtok.network.ApiMode;
import com.crewtok.network.ApiResponse;
import com.crewtok.util.AppPreference;
import com.google.android.gms.location.*;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class LocationTrackerService extends IntentService implements LocationListener, ApiResponse {

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            onLocationChanged(locationResult.getLastLocation());
        }
    };
    private BroadcastReceiver pauseLocationUpdateReceiver;


    public LocationTrackerService() {
        super("LocationTrackerService");
    }

    public LocationTrackerService(String name) {
        super(name);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        pauseLocationUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean shouldPause;
                if (intent != null) {
                    shouldPause = intent.getBooleanExtra("shouldPause", false);
                    if (shouldPause) {
                        stopLocationUpdates();
                    } else {
                        startLocationUpdates();
                    }
                }
            }
        };
        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(pauseLocationUpdateReceiver,
                        new IntentFilter("pauseLocationUpdates"));
        return START_STICKY;
    }

    protected void startLocationUpdates() {
        // Create the location request to start receiving updates
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(0.0f);
        mLocationRequest.setExpirationTime(30000);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest,
                locationCallback,
                Looper.myLooper());
    }

    private void stopLocationUpdates() {
        getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {

            List<Address> list = null;
            try {
                list = new Geocoder(this, Locale.getDefault()).getFromLocation(location.getLatitude(),
                        location.getLongitude(), 1);
              String addr=  list.get(0).getAddressLine(0);
              updateLocationToServer(addr);
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    private void updateLocationToServer(String locationString) {
        Log.e("location name: ", locationString);

        HashMap<String, Object> map = new HashMap<>();
        map.put("userid", AppPreference.getInstance().getUserId());
        map.put("location", locationString);
        ApiManager.getInstance().requestApi(ApiMode.UPDATE_LIVE_LOCATION, map, false, this, "POST");
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext())
                .unregisterReceiver(pauseLocationUpdateReceiver);
        super.onDestroy();

    }

    @Override
    public void onSuccess(JsonObject jsonObject, ApiMode mode) {

    }

    @Override
    public void onFailure(JsonObject errorObject, ApiMode mode) {

    }

    @Override
    public void onException(Exception e, ApiMode mode) {

    }
}
