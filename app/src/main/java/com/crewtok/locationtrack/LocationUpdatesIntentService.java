package com.crewtok.locationtrack;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import androidx.core.content.ContextCompat;
import com.google.android.gms.location.LocationResult;

import java.util.List;

/**
 * Handles incoming location updates and displays a notification with the location data.
 * <p>
 * For apps targeting API level 25 ("Nougat") or lower, location updates may be requested
 * using {@link android.app.PendingIntent#getService(Context, int, Intent, int)} or
 * {@link android.app.PendingIntent#getBroadcast(Context, int, Intent, int)}. For apps targeting
 * API level O, only {@code getBroadcast} should be used.
 * <p>
 * Note: Apps running on "O" devices (regardless of targetSdkVersion) may receive updates
 * less frequently than the interval specified in the
 * {@link com.google.android.gms.location.LocationRequest} when the app is no longer in the
 * foreground.
 */
public class LocationUpdatesIntentService extends IntentService {

    static final String ACTION_PROCESS_UPDATES =
            "com.google.android.gms.location.sample.backgroundlocationupdates.action" +
                    ".PROCESS_UPDATES";
    private static final String TAG = LocationUpdatesIntentService.class.getSimpleName();


    public LocationUpdatesIntentService() {
        // Name the worker thread.
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(1234, getNotification(this));
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROCESS_UPDATES.equals(action)) {
                LocationResult result = LocationResult.extractResult(intent);
                if (result != null) {
                    List<Location> locations = result.getLocations();
                    LocationResultHelper locationResultHelper = new LocationResultHelper(this,
                            locations);
                    // Save the location data to SharedPreferences.
                    locationResultHelper.saveResults();
                    // Show notification with the location data.
//                    locationResultHelper.showNotification();
//                    Log.i(TAG, LocationResultHelper.getSavedLocationResult(this));
                }
            }
        }
    }

    private Notification getNotification(Context context) {

        NotificationChannel channel = null;
        Notification.Builder builder = null;
        NotificationManager notificationManager = ContextCompat.getSystemService(context, NotificationManager.class);

        if (Build.VERSION.SDK_INT > 26) {
            channel = new NotificationChannel(
                    "channel_1",
                    "My Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }

            builder = new Notification.Builder(getApplicationContext(), "channel_1");

        } else {
            builder = new Notification.Builder(getApplicationContext());
        }

        return builder.build();
    }
}
