package com.crewtok.locationtrack;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.os.*;
import android.util.Log;
import androidx.core.content.ContextCompat;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.crewtok.network.ApiManager;
import com.crewtok.ui.dashboard.DashboardActivity;
import com.crewtok.util.AppPreference;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class BackgroundService extends Service {
    private final LocationServiceBinder binder = new LocationServiceBinder();
    private final String TAG = "BackgroundService";
    //    private LocationListener mLocationListener;
//    private LocationManager mLocationManager;
    private NotificationManager notificationManager;

    private final int LOCATION_INTERVAL = 10000;
    private final int LOCATION_DISTANCE = 0;

    private LocationCallback locationCallback = new LocationCallback() {
        @SuppressLint("StaticFieldLeak")
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            new AsyncTask<Location, Void, String>() {
                @Override
                protected String doInBackground(Location... location) {

                    String locationString = null;
                    try {
                        locationString = new Geocoder(getApplicationContext()).getFromLocation(
                                location[0].getLatitude(), location[0].getLongitude(), 1
                        ).get(0).getAddressLine(0);
                        Log.e("location result", locationString);

                    } catch (Exception e) {

                    }
                    return locationString;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    if (AppPreference.getInstance().getLoggedIn()) {
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("userid", AppPreference.getInstance().getUserId());
                        map.put("location", s);
                        AndroidNetworking.post(ApiManager.BASE_URL + "updatelivestatus")
                                .addQueryParameter(map)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                    }

                                    @Override
                                    public void onError(ANError anError) {

                                    }
                                });

                    }
                }
            }.execute(locationResult.getLastLocation());
        }


        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            super.onLocationAvailability(locationAvailability);
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }


//    private class LocationListener implements android.location.LocationListener {
//        private Location lastLocation = null;
//        private final String TAG = "LocationListener";
//        private Location mLastLocation;
//
//        public LocationListener(String provider) {
//            mLastLocation = new Location(provider);
//        }
//
//        @Override
//        public void onLocationChanged(Location location) {
//            mLastLocation = location;
//            Log.e(TAG, "LocationChanged: " + location);
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {
//            Log.e(TAG, "onProviderDisabled: " + provider);
//        }
//
//        @Override
//        public void onProviderEnabled(String provider) {
//            Log.e(TAG, "onProviderEnabled: " + provider);
//        }
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//            Log.e(TAG, "onStatusChanged: " + status);
//        }
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        notificationManager = ContextCompat.getSystemService(this, NotificationManager.class);
        startForeground(12345678, getNotification());
    }

    @Override
    public void onDestroy() {
        try {
            stopForeground(true);
            notificationManager.cancelAll();
            getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback);

        } catch (Exception ex) {
            Log.i(TAG, "fail to remove location listners, ignore", ex);
        } finally {
            super.onDestroy();
        }
    }
//
//    private void initializeLocationManager() {
////        if (mLocationManager == null) {
////            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
////        }
//    }

    public void startTracking() {
//        initializeLocationManager();
//        mLocationListener = new LocationListener(LocationManager.GPS_PROVIDER);

        try {

            LocationRequest mLocationRequest = new LocationRequest();

            mLocationRequest.setInterval(LOCATION_INTERVAL);
            mLocationRequest.setFastestInterval(0);
            mLocationRequest.setFastestInterval(LocationRequest.PRIORITY_HIGH_ACCURACY);

            getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper());

//
//            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
//                    LOCATION_INTERVAL, LOCATION_DISTANCE, mLocationListener);

        } catch (java.lang.SecurityException ex) {
            // Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            // Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }

    }

    public void stopTracking() {
        this.onDestroy();

    }


    private Notification getNotification() {

        NotificationChannel channel = null;
        Notification.Builder builder = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel("channel_01", "My Channel", NotificationManager.IMPORTANCE_DEFAULT);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
            builder = new Notification.Builder(getApplicationContext(), "channel_01").setAutoCancel(true);
        } else {
            builder = new Notification.Builder(this);
        }
        Intent intent = new Intent(this, DashboardActivity.class);
        builder.setContentIntent(PendingIntent.getActivity(this, 12345, intent, PendingIntent.FLAG_UPDATE_CURRENT));

        return builder.build();
    }


    public class LocationServiceBinder extends Binder {
        public BackgroundService getService() {
            return BackgroundService.this;
        }
    }

}
