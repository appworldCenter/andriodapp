package com.crewtok.locationtrack;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;
import androidx.core.app.TaskStackBuilder;
import com.crewtok.R;
import com.crewtok.network.ApiManager;
import com.crewtok.network.ApiMode;
import com.crewtok.network.ApiResponse;
import com.crewtok.ui.dashboard.DashboardActivity;
import com.crewtok.util.AppPreference;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Class to process location results.
 */
public class LocationResultHelper implements ApiResponse {

    public final static String KEY_LOCATION_UPDATES_RESULT = "location-update-result";

    final private static String PRIMARY_CHANNEL = "default";


    private Context mContext;
    private List<Location> mLocations;
    private NotificationManager mNotificationManager;

    LocationResultHelper(Context context, List<Location> locations) {
        mContext = context;
        mLocations = locations;

        NotificationChannel channel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel(PRIMARY_CHANNEL,
                    context.getString(R.string.default_channel), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setLightColor(Color.GREEN);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            getNotificationManager().createNotificationChannel(channel);
        }

    }

    /**
     * Returns the title for reporting about a list of {@link Location} objects.
     */
    private String getLocationResultTitle() {
//        String numLocationsReported = mContext.getResources().getQuantityString(
//                R.plurals.num_locations_reported, mLocations.size(), mLocations.size());
        return "";
    }

    private String getLocationResultText() {
        if (mLocations.isEmpty()) {
            return "Unknwon location";
        }

        return getLocationAddress(mLocations.get(mLocations.size() - 1));
    }

    private String getLocationAddress(Location location) {
        try {
            List<Address> list = new Geocoder(mContext, Locale.getDefault()).getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
            return list.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Saves location result as a string to {@link android.content.SharedPreferences}.
     */
    void saveResults() {

//        PreferenceManager.getDefaultSharedPreferences(mContext)
//                .edit()
//                .putString(KEY_LOCATION_UPDATES_RESULT, getLocationResultTitle() + "\n" +
//                        getLocationResultText())
//                .apply();

        String locationString = getLocationResultText();
        Log.e("location name: ", locationString);

        HashMap<String, Object> map = new HashMap<>();
        map.put("userid", AppPreference.getInstance().getUserId());
        map.put("location", locationString);
        ApiManager.getInstance().requestApi(ApiMode.UPDATE_LIVE_LOCATION, map, false, this, "POST");


    }

    /**
     * Fetches location results from {@link android.content.SharedPreferences}.
     */
    static String getSavedLocationResult(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_LOCATION_UPDATES_RESULT, "");
    }

    /**
     * Get the notification mNotificationManager.
     * <p>
     * Utility method as this helper works with it a lot.
     *
     * @return The system service NotificationManager
     */
    private NotificationManager getNotificationManager() {
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) mContext.getSystemService(
                    Context.NOTIFICATION_SERVICE);
        }
        return mNotificationManager;
    }

    /**
     * Displays a notification with the location results.
     */
    void showNotification() {
        Intent notificationIntent = new Intent(mContext, DashboardActivity.class);

        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(DashboardActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder notificationBuilder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationBuilder = new Notification.Builder(mContext,
                    PRIMARY_CHANNEL);
        } else {
            notificationBuilder = new Notification.Builder(mContext);
        }
        notificationBuilder.setContentTitle("Crewtok")
                .setContentText("Location updated")
                .setSmallIcon(R.drawable.crewtok_logo)
                .setAutoCancel(true)
                .setContentIntent(notificationPendingIntent);


        getNotificationManager().notify(0, notificationBuilder.build());
    }

    void hideNotification() {
        getNotificationManager().cancel(0);
    }

    @Override
    public void onSuccess(JsonObject jsonObject, ApiMode mode) {

    }

    @Override
    public void onFailure(JsonObject errorObject, ApiMode mode) {

    }

    @Override
    public void onException(Exception e, ApiMode mode) {

    }
}
