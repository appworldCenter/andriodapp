package com.crewtok.core

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.BuildConfig
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.crashlytics.android.Crashlytics
import com.crewtok.locationtrack.BackgroundService
import io.fabric.sdk.android.Fabric

class MyApplication : Application() {

    private lateinit var context: AppCompatActivity

    companion object {
        lateinit var instance: MyApplication
    }

    protected var mIsServiceBound = false
    protected var gpsService: BackgroundService? = null
    protected val serviceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            if (name?.className.equals("BackgroundService")) {
                gpsService = null
            }
        }

        override fun onServiceConnected(component: ComponentName?, service: IBinder?) {
            val name = component?.className ?: return
            if (name.endsWith("BackgroundService")) {
                gpsService = (service as BackgroundService.LocationServiceBinder).service
                gpsService?.startTracking()
            }
        }

    }

    override fun onCreate() {
        instance = this
        super.onCreate()
        Fabric.with(this, Crashlytics())
        AndroidNetworking.initialize(this)
//        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.HEADERS)
    }

    fun setContext(ctx: AppCompatActivity) {
        context = ctx
    }

    fun getContext(): AppCompatActivity {
        return context
    }

    public fun startLocationTracking() {
        val intent = Intent(this, BackgroundService::class.java)
        startService(intent)

        mIsServiceBound = bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)

    }

    public fun stopAndUnbind() {
        if (mIsServiceBound) {
            try {
                unbindService(serviceConnection)
                stopService(Intent(this, BackgroundService::class.java))
                gpsService?.stopTracking()

                mIsServiceBound = false
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

//
    }
}

