package com.crewtok.ui.fullimage

import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import kotlinx.android.synthetic.main.activity_full_screen_image.*

class FullScreenImageActivity : BaseDrawerActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image)

        loadImage()

        btnBackTouchImageView.setOnClickListener {
           onBackPressed()
        }
    }

    private fun loadImage() {
        if (intent?.hasExtra("image")!!) {
            Glide.with(this).load(
                intent.getStringExtra("image")
            )
                .apply(
                    RequestOptions().placeholder(R.drawable.image_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                )
                .into(touchImageView)
        }
    }
}
