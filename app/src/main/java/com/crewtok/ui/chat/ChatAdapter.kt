package com.crewtok.ui.chat

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.crewtok.R
import com.crewtok.base.BaseViewHolder
import com.crewtok.data.ItemChatModel
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import kotlinx.android.synthetic.main.item_message_received.view.*
import kotlinx.android.synthetic.main.item_message_sent.view.*

class ChatAdapter(val requestManager: RequestManager) : RecyclerView.Adapter<ChatAdapter.ChatVh>() {

    val list = ArrayList<ItemChatModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatVh {

        return ChatVh(
            LayoutInflater.from(parent.context)
                .inflate(viewType, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ChatVh, position: Int) {
        holder.bind(list[holder.adapterPosition])
    }

    fun addAll(data: ArrayList<ItemChatModel>) {

        data.forEach {
            if (!list.contains(it)) {
                list.add(it)
                notifyItemInserted(list.size)
            }

        }
    }

    override fun getItemId(position: Int): Long {
        return list[position].messageId.toLong()
    }

    fun getLastChatId(): Int {
        return list[list.size - 1].messageId
    }


//    override fun onViewRecycled(holder: ChatVh) {
//        if (getItemViewType(holder.adapterPosition) == R.layout.item_message_sent) {
//            requestManager.clear(holder.itemView.itemSenderImageView)
//        } else {
//            requestManager.clear(holder.itemView.itemReceiverImage)
//        }
//    }

    override fun getItemViewType(position: Int): Int =
        if (list[position].sender == AppPreference.getInstance().userId) {
            R.layout.item_message_sent
        } else R.layout.item_message_received


    inner class ChatVh(view: View) : BaseViewHolder(view) {
        override fun onClick(v: View?) {
        }

        fun bind(model: ItemChatModel) {
            if (itemViewType == R.layout.item_message_sent) {
                itemView.itemSenderMessage.text = Html.fromHtml(model.message?.trim())
                itemView.itemSenderTime.text = model.Time
                requestManager.load((model.Image))
                    .apply(
                        RequestOptions().placeholder(R.drawable.image_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                    )
                    .into(itemView.itemSenderImageView)

            } else {
                itemView.itemReceiverMessage.text = Html.fromHtml(model.message?.trim())
                itemView.itemReceiverTime.text = model.Time
                requestManager.load((model.Image))
                    .apply(RequestOptions().placeholder(R.drawable.image_placeholder))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.itemReceiverImage)

            }
        }

    }
}