package com.crewtok.ui.chat

import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.data.ItemChatModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.item_toolbar.*
import java.lang.Exception

class ChatActivity : BaseDrawerActivity(), ApiResponse {


    private var contactId = -1
    private var page = 1

    private lateinit var chatAdapter: ChatAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCustomContentView(R.layout.activity_chat)

        toolbarTitle.text = intent?.getStringExtra("name")
        contactId = intent?.getIntExtra("contactid", 0)!!
        findViewById<ImageView>(R.id.imageView4).visibility = View.VISIBLE

        requestChatListApi()
        setupChatList()
        setupClickListeners()
    }

    private fun setupClickListeners() {
        findViewById<ImageView>(R.id.imageView4).setOnClickListener {
            onBackPressed()
        }
        btnSendChatMessage.setOnClickListener {
            requestAddChatApi()
        }
    }

    private fun requestChatListApi() {
        val map = HashMap<String, Any>()
        map["userid"] = AppPreference.getInstance().userId
        map["contactid"] = contactId
        map["page"] = page

        ApiManager.getInstance().requestApi(ApiMode.CHAT_HISTORY, map, false, this, "GET")
    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {

        val typeToken = object : TypeToken<ArrayList<ItemChatModel>>() {}.type
        when (mode) {
            ApiMode.CHAT_HISTORY -> {
                progressBar.visibility = View.GONE
                val list = Gson().fromJson<ArrayList<ItemChatModel>>(
                    jsonObject?.getAsJsonObject("data")
                        ?.getAsJsonArray("msgItem"), typeToken
                )
                if (list.isEmpty())
                    AppUtils.shortToast("No chat found")
                else {
                    chatAdapter.addAll(list)
                    chatMessageList.smoothScrollToPosition(chatAdapter.list.size - 1)
                }

                handler.postDelayed(runnable, 10000)

            }
            ApiMode.ADD_CHAT -> {
                etChatMessageEditor.setText("")
                val list = Gson().fromJson<ArrayList<ItemChatModel>>(
                    jsonObject?.getAsJsonObject("data")
                        ?.getAsJsonArray("msgItem"), typeToken
                )

                chatAdapter.addAll(list)
                chatMessageList.smoothScrollToPosition(chatAdapter.list.size - 1)
                handler.postDelayed(runnable, 10000)

            }

            ApiMode.AJAX_CHAT -> {

                val list = Gson().fromJson<ArrayList<ItemChatModel>>(
                    jsonObject?.getAsJsonObject("data")
                        ?.getAsJsonArray("msgItem"), typeToken
                )

                chatAdapter.addAll(list)
                chatMessageList.smoothScrollToPosition(chatAdapter.list.size - 1)
                handler.postDelayed(runnable, 5000)

            }
            ApiMode.CLOCKIN -> {
                launchTimeTrackDialog(
                    jsonObject?.getAsJsonObject("data")?.get("isclockin")?.asBoolean!!,
                    jsonObject.getAsJsonObject("data")?.get("lastmessage")?.asString
                )
            }
        }
    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        e?.printStackTrace()
       // AppUtils.showException()
    }

    private fun setupChatList() {
        chatAdapter = ChatAdapter(Glide.with(this))
        chatAdapter.setHasStableIds(true)
        chatMessageList.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = chatAdapter
        }
    }

    private fun requestAddChatApi() {
        handler.removeCallbacks(runnable)
        if (!TextUtils.isEmpty(etChatMessageEditor.text.toString())) {
            val map = HashMap<String, Any>()
            map["userid"] = AppPreference.getInstance().userId
            map["contactid"] = contactId
            map["Text"] = etChatMessageEditor.text.toString()

            ApiManager.getInstance().requestApi(ApiMode.ADD_CHAT, map, true, this, "POST")
        }
    }

    private val handler = Handler()
    private val runnable = Runnable {

        var maxId = 0
        if (chatAdapter.list.isNotEmpty()) {
            maxId = chatAdapter.getLastChatId()
        }

        val map = HashMap<String, Any>()
        map["userid"] = AppPreference.getInstance().userId
        map["contactid"] = contactId
        map["maxid"] = maxId

        ApiManager.getInstance().requestApi(ApiMode.AJAX_CHAT, map, false, this, "GET")


    }


    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
    }

}
