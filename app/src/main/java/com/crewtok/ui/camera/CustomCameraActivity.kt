package com.crewtok.ui.camera

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.crewtok.R
import com.crewtok.util.AppUtils
import com.otaliastudios.cameraview.CameraException
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import kotlinx.android.synthetic.main.activity_custom_camera.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class CustomCameraActivity : AppCompatActivity() {

    private var filePath = ""
    private var mode = 1

    private var resultt: PictureResult? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_camera)
        configureCamera()
        setupCameraListener()
        setupClickListener()
        switchMode()
    }

    private fun setupClickListener() {
        btnExitCamera.setOnClickListener {
            finish()
        }

        btnTakePicture.setOnClickListener {
            customCamera.takePicture()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            filePath = absolutePath
        }
    }

    private fun configureCamera() {
        customCamera.setLifecycleOwner(this)
        customCamera.useDeviceOrientation
    }

    private fun setupCameraListener() {
        customCamera.addCameraListener(object : CameraListener() {

            override fun onPictureTaken(result: PictureResult) {
                if (!TextUtils.isEmpty(filePath)) {
                    resultt = result
                    mode = 2
                    switchMode()
                }

            }

            override fun onCameraError(exception: CameraException) {
                super.onCameraError(exception)
                exception.printStackTrace()
                AppUtils.showException()
            }

        })

    }

    override fun onResume() {
        super.onResume()
        customCamera.open()
    }

    override fun onPause() {
        super.onPause()
        customCamera.close()
    }

    override fun onDestroy() {
        super.onDestroy()
        customCamera.destroy()
    }

    private fun switchMode() {

        if (mode == 1) {
            createImageFile()
            cameraModeLayout.visibility = View.VISIBLE
            customCamera.open()
            previewModeLayout.visibility = View.GONE

        } else {
            // launch preview
            cameraModeLayout.visibility = View.GONE
            customCamera.close()
            previewModeLayout.visibility = View.VISIBLE

            Glide.with(this).load((resultt?.data)).into(previewImageView)

            btnRetake.setOnClickListener {
                mode = 1
                switchMode()
            }

            btnConfirm.setOnClickListener {
                val res = Intent().apply {
                    putExtra("filePath", filePath)
                }
                resultt?.let {
                    it.toFile(File(filePath)) {
                        setResult(Activity.RESULT_OK, res)
                        finish()
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        if (mode == 2) {
            mode = 1
            switchMode()
        } else {
            super.onBackPressed()
        }
    }
}
