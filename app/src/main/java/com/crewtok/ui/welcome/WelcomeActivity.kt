package com.crewtok.ui.welcome

import Router
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import com.crewtok.BuildConfig
import com.crewtok.R
import com.crewtok.base.BaseActivity
import com.crewtok.ui.login.LoginActivity
import com.crewtok.util.AppUtils
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : BaseActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        tvVersionName.text = """${BuildConfig.VERSION_NAME}"""

        findViewById<AppCompatButton>(R.id.btnSignInWelcome)
            .setOnClickListener(this)
        findViewById<View>(R.id.tvRequestAnAccount)
            .setOnClickListener(this)
        findViewById<View>(R.id.tvSupport)
            .setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSignInWelcome -> {
                Router.start<LoginActivity>()
                finish()
            }
            R.id.tvRequestAnAccount -> {
//                AppUtils.openGmail("Request an account", "admin@crewtok.com")
                AppUtils.openBrowser("https://crewtok.com/")
            }
            R.id.tvSupport -> {
                AppUtils.openBrowser("https://crewtok.com/contact.html")
//                AppUtils.openGmail("Crewtok support", "admin@crewtok.com")
            }
        }

    }
}
