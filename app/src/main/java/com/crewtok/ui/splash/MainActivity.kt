package com.crewtok.ui.splash

import android.os.Bundle
import android.os.Handler
import androidx.core.app.ActivityCompat
import com.crewtok.R
import com.crewtok.base.BaseActivity
import com.crewtok.ui.dashboard.DashboardActivity
import com.crewtok.ui.welcome.WelcomeActivity
import com.crewtok.util.AppConstants
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.crewtok.util.launchActivity

class MainActivity : BaseActivity() {

    private var runnable: Runnable? = null
    private var handler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // request for permissions
        ActivityCompat.requestPermissions(this, AppConstants.ALL_PERMISSIONS, 123)

        /* initializing handler */
        handler = Handler()

        /* initializing runnable code to be executed after permissions are satisfied */
        runnable = Runnable {
            if (AppPreference.getInstance().loggedIn) {
                launchActivity<DashboardActivity> { }
            } else {
                launchActivity<WelcomeActivity> { }
            }
            finish()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 123) {
            var allPermit = 1
            grantResults.forEach {
                if (it == -1)
                    allPermit *= 0
            }
            if (allPermit == 1) {
                handler?.postDelayed(runnable, 3000)
            } else {
                /* add a dialog stating why these permissions are necessary */
                AppUtils.shortToast("Cannot proceed without these permissions")
            }

        }
    }
}
