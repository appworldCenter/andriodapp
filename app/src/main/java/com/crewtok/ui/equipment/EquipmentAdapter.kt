package com.crewtok.ui.equipment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.crewtok.R
import com.crewtok.base.BaseViewHolder
import com.crewtok.data.EquipmentModel
import kotlinx.android.synthetic.main.item_assigned_equipment.view.*

class EquipmentAdapter : RecyclerView.Adapter<EquipmentAdapter.EquipmentVh>() {
    private val list = ArrayList<EquipmentModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EquipmentVh {
        return EquipmentVh(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_assigned_equipment,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: EquipmentVh, position: Int) {
        holder.bind(list[holder.adapterPosition])
    }

    fun addEquipments(data: ArrayList<EquipmentModel>) {
        list.addAll(data)
        notifyDataSetChanged()
    }


    class EquipmentVh(itemView: View) : BaseViewHolder(itemView) {


        fun bind(model: EquipmentModel) {
            itemView.equipmentDate.text = "${model.date} ${model.time}"
            itemView.equipmentName.text = "${model.name} ${model.serialnumber}"
            itemView.equipmentAssignedBy.text = "Assigned by: ${model.assignedby}"
        }


        override fun onClick(v: View?) {
            // not used
        }
    }
}