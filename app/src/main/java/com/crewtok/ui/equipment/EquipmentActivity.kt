package com.crewtok.ui.equipment

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout.VERTICAL
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.data.EquipmentModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.ui.dashboard.DashboardActivity
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.crewtok.util.launchActivity
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_equipment.*
import kotlinx.android.synthetic.main.item_toolbar.*
import java.lang.Exception

class EquipmentActivity : BaseDrawerActivity(), ApiResponse {


    private lateinit var equipmentAdapter: EquipmentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCustomContentView(R.layout.activity_equipment)

        toolbarTitle.text = "Assigned Equipments"
        setupBackPress()

        requestEquipmentsApi().also {
            setupEquipmentsList()
        }


    }

    private fun requestEquipmentsApi() {
        val map = HashMap<String, Any>()
        map["userid"] = AppPreference.getInstance().userId
        ApiManager.getInstance().requestApi(ApiMode.EQUIPMENT_LIST, map, false, this, "GET")
    }


    private fun setupEquipmentsList() {

        equipmentAdapter = EquipmentAdapter()
        assignedEquipmentsList.apply {
            layoutManager = LinearLayoutManager(this@EquipmentActivity)
            addItemDecoration(DividerItemDecoration(this@EquipmentActivity, VERTICAL))
            adapter = equipmentAdapter
        }
    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {

        when (mode) {
            ApiMode.CLOCKIN -> {
                launchTimeTrackDialog(
                    jsonObject?.getAsJsonObject("data")?.get("isclockin")?.asBoolean!!,
                    jsonObject?.getAsJsonObject("data")?.get("lastmessage")?.asString
                )
            }
            ApiMode.EQUIPMENT_LIST -> {
                progressBar.visibility = View.GONE
                val typeToken = object : TypeToken<ArrayList<EquipmentModel>>() {}.type
                val list = Gson().fromJson<ArrayList<EquipmentModel>>(jsonObject?.getAsJsonArray("data"), typeToken)
                if (list.isEmpty())
                    AppUtils.shortToast("No Equipments available")
                else
                    equipmentAdapter.addEquipments(list)
            }
        }

    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        AppUtils.showException()
    }

    override fun onBackPressed() {
        launchActivity<DashboardActivity> {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }
}
