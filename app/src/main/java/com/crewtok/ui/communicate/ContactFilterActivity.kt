package com.crewtok.ui.communicate

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.data.ItemContactGroupModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_contact_filter.*
import kotlinx.android.synthetic.main.item_contact_group.view.*

class ContactFilterActivity : BaseDrawerActivity(), ApiResponse {


    private lateinit var contactGroupAdapter: ContactsFilterAdapter


    var selectedGroupId = -1
    var selectedGroupName = "All"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_filter)

        intent.let {
            selectedGroupId = it.getIntExtra("selectedGroupId", -1)
            selectedGroupName = it.getStringExtra("selectedGroupName")
        }

        setupToolbar()
        setupContactGroupList()
        requestContactGroupListApi()
        setupApplyClick()
    }


    private fun setupToolbar() {
        contactGroupToolbar.setNavigationOnClickListener {
            finish()
        }
    }

    private fun requestContactGroupListApi() {
        val map = HashMap<String, Any>()
        map["UserId"] = AppPreference.getInstance().userId
        ApiManager.getInstance().requestApi(ApiMode.CONTACT_GROUP_LIST, map, false, this, "GET")
    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {
        progressBar.visibility = View.GONE

        val typeToken = object : TypeToken<ArrayList<ItemContactGroupModel>>() {}.type
        val list = Gson().fromJson<ArrayList<ItemContactGroupModel>>(
            jsonObject?.getAsJsonArray("data"),
            typeToken
        )

        list.add(0, ItemContactGroupModel(-1, "All", true))

        contactGroupAdapter.addGroups(list)

    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        e?.printStackTrace()
        AppUtils.showException()
    }

    private fun setupApplyClick() {
        btnApplyFilter.setOnClickListener {
            //            CommunicateActivity.selectedGroupId = contactGroupAdapter.selectedId
//            CommunicateActivity.selectedGroupName = contactGroupAdapter.selectedName
            val intent = Intent().apply {
                putExtra("selectedGroupId", contactGroupAdapter.selectedId)
                putExtra("selectedGroupName", contactGroupAdapter.selectedName)
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun setupContactGroupList() {
        contactGroupAdapter = ContactsFilterAdapter(selectedGroupId, selectedGroupName)
        contactGroupRecycler.apply {
            adapter = contactGroupAdapter
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        }
    }
}


class ContactsFilterAdapter(val id: Int, val name: String) :
    RecyclerView.Adapter<ContactsFilterAdapter.ContactGroupVh>() {

    var selectedId = id
    var selectedName = name

    val list = ArrayList<ItemContactGroupModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactGroupVh {
        return ContactGroupVh(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_contact_group,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ContactGroupVh, position: Int) {
        holder.bind(list[holder.adapterPosition])
    }

    fun addGroups(data: ArrayList<ItemContactGroupModel>) {
        list.addAll(data)
        notifyDataSetChanged()
    }

    inner class ContactGroupVh(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: ItemContactGroupModel) {
            itemView.itemContactGroupText.text = model.name

            if (selectedId == model.id) {
                itemView.itemContactGroupText
                    .setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_checked, 0)
            } else {
                itemView.itemContactGroupText
                    .setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_unchecked, 0)
            }

            itemView.setOnClickListener {
                selectedId = model.id
                selectedName = model.name
                notifyDataSetChanged()
            }
        }
    }

}
