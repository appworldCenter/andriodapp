package com.crewtok.ui.communicate

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.data.ContactModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.ui.chat.ChatActivity
import com.crewtok.ui.dashboard.DashboardActivity
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.crewtok.util.launchActivity
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_communicate.*

class CommunicateActivity : BaseDrawerActivity(), OnRecyclerViewItemClick<ContactModel>,
    ApiResponse {


    private lateinit var contactAdapter: ContactsAdapter

    //    companion object {
    private var selectedGroupId = -1
    private var selectedGroupName = "All"
//    }

    val TITLE = "Contact"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCustomContentView(R.layout.activity_communicate)
        setTitleBar()
        findViewById<ImageView>(R.id.imageView4)
            .setOnClickListener {
                onBackPressed()
            }
        requestContactsApi(null).also {
            setupContactsList()
        }

        btnSelectCategory.setOnClickListener {
            launchActivity<ContactFilterActivity>(204) {
                putExtra("selectedGroupId",selectedGroupId)
                putExtra("selectedGroupName",selectedGroupName)
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        selectedGroupId=-1
        selectedGroupName="All"
        setTitleBar()
        requestContactsApi(null)
    }

    private fun setTitleBar() {
        textView12.text = "$TITLE ($selectedGroupName)"
    }

    private fun requestContactsApi(groupId: String?) {
        progressBar.visibility = View.VISIBLE
        val map = HashMap<String, Any>()
        map["UserId"] = AppPreference.getInstance().userId
        if (groupId != null && groupId != "-1")
            map["groupid"] = groupId
        ApiManager.getInstance().requestApi(ApiMode.CONTACT_LIST, map, false, this, "GET")

    }

    /* Setup contacts list */
    private fun setupContactsList() {

        contactAdapter = ContactsAdapter(this, Glide.with(this))
        contactAdapter.setHasStableIds(true)
        communicationList.apply {
            layoutManager = LinearLayoutManager(this@CommunicateActivity)
            addItemDecoration(
                DividerItemDecoration(
                    this@CommunicateActivity,
                    LinearLayout.VERTICAL
                )
            )
            adapter = contactAdapter
        }

    }

    /* Handle item click callback */
    override fun onRecyclerItemClicked(pos: Int, view: View, data: ContactModel) {
        launchActivity<ChatActivity> {
            putExtra("contactid", data.id)
            putExtra("name", data.name)
        }
    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {
        progressBar.visibility = View.GONE
        val typeToken = object : TypeToken<ArrayList<ContactModel>>() {}.type
        val list =
            Gson().fromJson<ArrayList<ContactModel>>(jsonObject?.getAsJsonArray("data"), typeToken)
        if (list.isEmpty())
            AppUtils.shortToast("No Contacts available")
        else
            contactAdapter.addContacts(list)
    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        e?.printStackTrace()
        AppUtils.showException()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 204 && resultCode == Activity.RESULT_OK && data != null) {
            selectedGroupId = data.getIntExtra("selectedGroupId", -1)
            selectedGroupName = data.getStringExtra("selectedGroupName")
            setTitleBar()
            requestContactsApi(selectedGroupId.toString())
        }
    }

    override fun onBackPressed() {
        launchActivity<DashboardActivity> {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }
}
