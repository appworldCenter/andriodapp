package com.crewtok.ui.communicate

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.crewtok.R
import com.crewtok.base.BaseViewHolder
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.data.ContactModel
import kotlinx.android.synthetic.main.item_communicate.view.*

class ContactsAdapter(
    val callback: OnRecyclerViewItemClick<ContactModel>,
    val requestManager: RequestManager
) :
    RecyclerView.Adapter<ContactsAdapter.ContactVh>() {

    private val list = ArrayList<ContactModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactVh {
        return ContactVh(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_communicate,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ContactVh, position: Int) {
        holder.bind(list[holder.adapterPosition])
    }

    fun addContacts(data: ArrayList<ContactModel>) {

        clearDataSet()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onViewRecycled(holder: ContactVh) {
        requestManager.clear(holder.itemView.contactImage)
    }

    fun clearDataSet() {
        list.clear()
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return list[position].id.toLong()
    }


    inner class ContactVh(itemView: View) : BaseViewHolder(itemView) {

        private var model: ContactModel? = null
        fun bind(model: ContactModel) {
            this.model = model
            itemView.contactName.text = model.name
            itemView.contactCategory.text = model.pgroup
            requestManager.load(model.image).apply(
                RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)
            ).into(itemView.contactImage)

            itemView.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            callback.onRecyclerItemClicked(adapterPosition, itemView, model!!)
        }
    }
}