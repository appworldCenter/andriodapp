package com.crewtok.ui.locationselect

import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.text.TextUtils
import android.view.Window
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.ui.assignment.sort.SortAssignmentActivity
import com.crewtok.util.AppUtils
import com.crewtok.util.TbmCurrentLocation
import com.crewtok.util.launchActivity
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import kotlinx.android.synthetic.main.activity_location_selection.*
import java.util.*


class LocationSelectionActivity : BaseDrawerActivity(), TbmCurrentLocation.CurrentLocationCallback {


    lateinit var placesClient: PlacesClient
    private var selectedLocation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_selection)
        requestCurrentLocation()
        initClickListeners()
        initPlacesClient()
    }

    private fun initPlacesClient() {
        Places.initialize(this, "AIzaSyA1LyE18eEmk5UdDLezPmcOPF47Diugxls")
        placesClient = Places.createClient(this)
    }

    private fun initClickListeners() {
        tvAnotherLocationSelect.setOnClickListener {

            val fields = listOf(Place.Field.ID, Place.Field.NAME)

            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 1234)

        }

        btnSubmitForNavigation.setOnClickListener {
            val i = Intent()
            i.putExtra("address", selectedLocation)
            setResult(Activity.RESULT_OK, i)
            finish()
        }

        dialogClose.setOnClickListener {
            finish()
        }
    }

    private fun requestCurrentLocation() {
        TbmCurrentLocation().fetchLocation(this)
    }

    override fun currentLocationFetched(lat: Double, lng: Double) {

        val address = Geocoder(this, Locale.getDefault())
            .getFromLocation(lat, lng, 1)[0].getAddressLine(0)

        if (!TextUtils.isEmpty(address.toString())) {
            selectedLocation = address
            txtCurrentLocation.text = "${txtCurrentLocation.text}\n${address}"

        } else {
            AppUtils.shortToast("Cannot find address")
        }
    }

    override fun currentLocationFetchFailed(error: String?) {
        AppUtils.shortToast(error)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1234) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                tvCurrentLocationSelect.text = " "
                tvAnotherLocationSelect.text = "X"
                tvAnotherLocation.text = "${tvAnotherLocation.text}\n${place.name}"
                selectedLocation = place.name!!
//                Log.i(FragmentActivity.TAG, "Place: " + place.name + ", " + place.id)
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
//                val status = Autocomplete.getStatusFromIntent(data)
//                Log.i(FragmentActivity.TAG, status.statusMessage)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
