package com.crewtok.ui.dashboard

import Router
import android.app.ActivityManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.text.Html
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.annotation.NonNull
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.core.MyApplication
import com.crewtok.data.DashboardModel
import com.crewtok.locationtrack.LocationRequestHelper
import com.crewtok.locationtrack.LocationResultHelper
import com.crewtok.locationtrack.LocationTrackerService
import com.crewtok.locationtrack.LocationUpdatesBroadcastReceiver
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.ui.assignment.AssignmentActivity
import com.crewtok.ui.communicate.CommunicateActivity
import com.crewtok.ui.equipment.EquipmentActivity
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.crewtok.util.TbmCurrentLocation
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.io.IOException


class DashboardActivity : BaseDrawerActivity(), OnRecyclerViewItemClick<String>, ApiResponse,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    SharedPreferences.OnSharedPreferenceChangeListener {


    internal val ACTION_PROCESS_UPDATES =
        "com.google.android.gms.location.sample.backgroundlocationupdates.action" + ".PROCESS_UPDATES"

    internal val ACTION_REMOVE_UPDATES =
        "com.google.android.gms.location.sample.backgroundlocationupdates.action" + ".REMOVE_UPDATES"


    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    // FIXME: 5/16/17
    private val UPDATE_INTERVAL = (10 * 1000).toLong()

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value, but they may be less frequent.
     */
    // FIXME: 5/14/17
    private val FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL / 2

    /**
     * The max time before batched results are delivered by location services. Results may be
     * delivered sooner than this interval.
     */
    private val MAX_WAIT_TIME = UPDATE_INTERVAL * 3

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private var mLocationRequest: LocationRequest? = null

    /**
     * The entry point to Google Play Services.
     */
    private var mGoogleApiClient: GoogleApiClient? = null


    lateinit var adapter: HomeAdapter
    //    private var currentLocationName = ""


    private val handler = Handler()

    private val runnable = Runnable {
        requestDashboardApi()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCustomContentView(com.crewtok.R.layout.activity_dashboard)
        setupNavHeaderViewData()
        setupHomeGrid()
        setupSendReply()

        MyApplication.instance.startLocationTracking()

    }

    override fun onConnected(p0: Bundle?) {
        requestLocationUpdates()
    }



    private fun requestDashboardApi() {
        val map = HashMap<String, Any>()
        map["userId"] = AppPreference.getInstance().userId
        ApiManager.getInstance().requestApi(ApiMode.DASHBOARD, map, true, this, "GET")
    }

    private fun setupHomeGrid() {
        adapter = HomeAdapter(this)
        homeRecyclerGrid.layoutManager = GridLayoutManager(this, 2)
        homeRecyclerGrid.adapter = adapter

//        requestDashboardApi()
    }

    private fun setupNavHeaderViewData() {
//        username.text = AppPreference.getInstance().userData.name
    }

    override fun onRecyclerItemClicked(pos: Int, view: View, data: String) {
        when (pos) {
            0 -> {
                handleTimeClock()
            }
            1 -> {
                Router.start<AssignmentActivity>()
            }
            2 -> {
                Router.start<CommunicateActivity>()
            }
            else -> {
                Router.start<EquipmentActivity>()
            }
        }
    }

    private fun handleTimeClock() {
        TbmCurrentLocation().fetchLocation(object : TbmCurrentLocation.CurrentLocationCallback {
            override fun currentLocationFetched(lat: Double, lng: Double) {
                val addresses = ArrayList<Address>()
                try {
                    val addr = Geocoder(this@DashboardActivity).getFromLocation(lat, lng, 1)
                    addresses.addAll(addr)
                } catch (ex: IOException) {

                }
                if (addresses.isNotEmpty()) {
                    currentLocationName = addresses[0].getAddressLine(0)
                    requestClockInApi()
                } else {
                    AppUtils.shortToast("Unable to find a valid location, Please try again")
                }
            }

            override fun currentLocationFetchFailed(error: String?) {
                AppUtils.shortToast("Cannot fetch location")
            }

        })
    }

    private fun requestClockInApi() {
        val map = HashMap<String, Any>()
        map["userId"] = AppPreference.getInstance().userId
        ApiManager.getInstance().requestApi(ApiMode.CLOCKIN, map, true, this, "POST")
    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {

        when (mode) {
            ApiMode.CLOCKIN -> {
                launchTimeTrackDialog(
                    jsonObject?.getAsJsonObject("data")?.get("isclockin")?.asBoolean!!,
                    jsonObject?.getAsJsonObject("data")?.get("lastmessage")?.asString
                )
            }

            ApiMode.DASHBOARD -> {

                dashboardModel = Gson().fromJson(
                    jsonObject?.getAsJsonObject("data"),
                    DashboardModel::class.java
                )

                dashboardLatestMessage.text = Html.fromHtml(dashboardModel?.latest_message)
                adapter.updateAssignedSubtitle(dashboardModel?.assignjobs!!)
                adapter.updateEquipmentSubtitle(dashboardModel?.equipments!!)
                adapter.updateUnreadChatCount(dashboardModel?.unreadchat!!)

                setupNavDrawer()

                handler.postDelayed(runnable, 5000)

            }

            ApiMode.ADD_CHAT -> {
                etAddReply.setText("")
                requestDashboardApi()
            }
        }


    }


    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        e?.printStackTrace()
       // AppUtils.showException()
    }

    override fun onPause() {
        handler.removeCallbacks(runnable)
        super.onPause()

    }


    override fun onResume() {
        handler.removeCallbacks(runnable)
        requestDashboardApi()
        super.onResume()

    }

    private fun setupSendReply() {
        etAddReply.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                requestAddChatApi()
            }
            true
        }

    }

    private fun requestAddChatApi() {
        if (!TextUtils.isEmpty(etAddReply.text.toString())) {
            val map = HashMap<String, Any>()
            map["userid"] = AppPreference.getInstance().userId
            map["contactid"] = dashboardModel?.chatcontactid!!
            map["Text"] = etAddReply.text.toString()

            ApiManager.getInstance().requestApi(ApiMode.ADD_CHAT, map, true, this, "POST")
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, s: String?) {
        if (s.equals(LocationResultHelper.KEY_LOCATION_UPDATES_RESULT)) {
//            mLocationUpdatesResultView.setText(LocationResultHelper.getSavedLocationResult(this));

        } else if (s.equals(LocationRequestHelper.KEY_LOCATION_UPDATES_REQUESTED)) {
//            updateButtonsState(LocationRequestHelper.getRequesting(this));
        }
    }

    private fun getPendingIntent(): PendingIntent {
        val intent = Intent(this, LocationUpdatesBroadcastReceiver::class.java)
        intent.action = ACTION_PROCESS_UPDATES
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }


    override fun onConnectionSuspended(i: Int) {
//        final String text = "Connection suspended";
//        Log.w(TAG, text + ": Error code: " + i);
//        showSnackbar("Connection suspended");
    }


    public override fun onConnectionFailed(@NonNull connectionResult: ConnectionResult) {
//        final String text = "Exception while connecting to Google Play services";
//        Log.w(TAG, text + ": " + connectionResult.getErrorMessage());
//        showSnackbar(text);
    }

    override fun onStart() {
        super.onStart()
        PreferenceManager.getDefaultSharedPreferences(this)
            .registerOnSharedPreferenceChangeListener(this)
    }

    override fun onStop() {
        PreferenceManager.getDefaultSharedPreferences(this)
            .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop()

    }


    private fun requestLocationUpdates() {
        try {
//            Log.i(TAG, "Starting location updates");
            LocationRequestHelper.setRequesting(this, true)
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, getPendingIntent()
            )
        } catch (e: SecurityException) {
            LocationRequestHelper.setRequesting(this, false)
            e.printStackTrace()
        }
    }

    /**
     * Handles the Remove Updates button, and requests removal of location updates.
     */
    fun removeLocationUpdates() {
        LocationRequestHelper.setRequesting(this, false)
        LocationServices.FusedLocationApi.removeLocationUpdates(
            mGoogleApiClient,
            getPendingIntent()
        )
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (runningService in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == runningService.service.getClassName()) {
                return true
            }
        }
        return false
    }


}
