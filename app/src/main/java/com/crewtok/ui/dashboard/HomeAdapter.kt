package com.crewtok.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.crewtok.R
import com.crewtok.base.BaseViewHolder
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.util.HomeOption
import com.crewtok.util.SampleData

class HomeAdapter(val callback: OnRecyclerViewItemClick<String>) : RecyclerView.Adapter<HomeAdapter.HomeVh>() {

    val list = SampleData.getHomeOptionList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeVh {
        return HomeVh(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_home,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HomeVh, position: Int) {
        holder.bind(list[holder.adapterPosition])

    }

    fun updateAssignedSubtitle(i: Int) {
        list[1].subtitle = "Assigned: $i"
        notifyItemChanged(1)
    }

    fun updateEquipmentSubtitle(i: Int) {
        list[3].subtitle = "Equipment: $i"
        notifyItemChanged(3)
    }

    fun updateUnreadChatCount(unreadChatCount: String) {
        list[2].subtitle = "Unread Chat: $unreadChatCount"
        notifyItemChanged(2)
    }

    inner class HomeVh(val view: View) : BaseViewHolder(view) {
        override fun onClick(v: View?) {

        }

        val bgImage = view.findViewById<ImageView>(R.id.itemHomeOptionImage)
        val title = view.findViewById<TextView>(R.id.homeOptionTitle)
        val subTitle = view.findViewById<TextView>(R.id.homeOptionSubtitle)

        fun bind(model: HomeOption) {
            title.text = model.title
            subTitle.text = model.subtitle
            Glide.with(view).load(model.image)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(bgImage)

            view.setOnClickListener {
                callback.onRecyclerItemClicked(adapterPosition, it, "")
            }
        }
    }
}