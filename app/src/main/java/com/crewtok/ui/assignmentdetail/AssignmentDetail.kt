package com.crewtok.ui.assignmentdetail

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bumptech.glide.Glide
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.data.AssignmentDetailModel
import com.crewtok.data.AttachmentModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.ui.assignmentdetail.allcomment.AllCommentsActivity
import com.crewtok.ui.fullimage.FullScreenImageActivity
import com.crewtok.ui.inventory.AddInventoryActivity
import com.crewtok.util.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import kotlinx.android.synthetic.main.activity_assignment_detail.*
import kotlinx.android.synthetic.main.dialog_description.view.*
import kotlinx.android.synthetic.main.dialog_description.view.dialogClose
import kotlinx.android.synthetic.main.dialog_view_contact.view.*
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.lang.Math.min
import java.util.*
import kotlin.collections.HashMap

class AssignmentDetail : BaseDrawerActivity(), ApiResponse,
    OnRecyclerViewItemClick<AttachmentModel> {

    private var jobId = 0
    private lateinit var picker: FilePickerBuilder
    private var fileOrImage = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCustomContentView(R.layout.activity_assignment_detail)
        val tvBack = findViewById<TextView>(R.id.tvBack)
        tvBack.setOnClickListener {
            onBackPressed()
        }
        if (intent?.hasExtra("assignment_id")!!) {
            jobId = intent?.getIntExtra("assignment_id", 0)!!
        }
        picker = FilePickerBuilder.instance.setMaxCount(1)
        requestJobDetailApi()
        setupClickListeners()
        setupAddAttachment()
    }

    private fun requestJobDetailApi() {

        jobDetailRoot.showKeyboard(false)

        jobDetailRoot.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
        val map = HashMap<String, Any>()
        map["jobid"] = jobId
        ApiManager.getInstance().requestApi(
            ApiMode.ASSIGNMENT_DETAIL, map,
            false, this, "GET"
        )
    }

    private fun requestJobContactApi() {
        val map = HashMap<String, Any>()
        map["jobid"] = jobId
        ApiManager.getInstance().requestApi(ApiMode.JOB_CONTACT, map, true, this, "GET")
    }

    private fun requestJobDescriptionApi() {
        val map = HashMap<String, Any>()
        map["jobid"] = jobId
        ApiManager.getInstance().requestApi(ApiMode.JOB_DESCRIPTION, map, true, this, "GET")
    }


    /* API RESPONSE */

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {

        when (mode) {
            ApiMode.ASSIGNMENT_DETAIL -> {
                val detail: AssignmentDetailModel =
                    Gson().fromJson(
                        jsonObject?.getAsJsonObject("data"),
                        AssignmentDetailModel::class.java
                    )
                setupData(detail)
                progressBar.visibility = View.GONE
                jobDetailRoot.visibility = View.VISIBLE
            }
            ApiMode.JOB_CONTACT -> {
                setupJobContactDialog(jsonObject?.getAsJsonObject("data"))
            }
            ApiMode.JOB_DESCRIPTION -> {
                setupJobDescriptionDialog(jsonObject?.get("data")?.asString)
            }
            ApiMode.UPDATE_JOB_STATUS -> {
                requestJobDetailApi()
            }
            ApiMode.CLOCKIN -> {
                launchTimeTrackDialog(
                    jsonObject?.getAsJsonObject("data")?.get("isclockin")?.asBoolean!!,
                    jsonObject?.getAsJsonObject("data")?.get("lastmessage")?.asString
                )
            }
            else -> {
            }
        }
    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        AppUtils.showException()
    }


    /* setup data to ui */
    private fun setupData(detail: AssignmentDetailModel) {
        jobTitle.text = detail.JobName
        jobTime.text = Html.fromHtml("<b>Time:</b> ${detail.time}")
        jobDate.text = Html.fromHtml("<b>Date: </b>${detail.date}")
        jobDetailLocation.text = Html.fromHtml("<b>Location: </b>${detail.Location}")
        jobDescription.text = Html.fromHtml("<b>Description: </b>" + detail.description).trim()
        jobInventory.text = detail.inventory.joinToString(", ", transform = {
            it.name
        })
        jobEquipments.text = detail.equipments.joinToString(", ", transform = {
            it.name
        })
        jobLatestComment.text = detail.latestComment


        // latest comment image

//        if (!TextUtils.isEmpty(detail.latestCommentimage)) {
//            Glide.with(this).load(detail.latestCommentimage)
//                .apply(
//                    RequestOptions().placeholder(R.drawable.image_placeholder)
//                        .encodeQuality(40)
//                )
//                .into(latestCommentImage)
//
//            latestCommentImage.setOnClickListener {
//                launchActivity<FullScreenImageActivity> {
//                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                    putExtra("image", detail.latestCommentimage)
//                }
//            }
//        }

        setupAttachments(detail.attachments)
        setupStatusLayout(detail.statusid)
    }

    private fun setupStatusLayout(statusid: Int) {
        when (statusid) {
            1 -> {
                jobStatusNew.setBackgroundResource(R.drawable.bg_status_new_solid)
                jobStatusNew.setTextColor(ContextCompat.getColor(this, R.color.colorFontWhite))

                jobStatusProgress.setBackgroundResource(R.drawable.bg_status_progress_stroke)
                jobStatusProgress.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusProgress
                    )
                )

                jobStatusProblem.setBackgroundResource(R.drawable.bg_status_problem_stroke)
                jobStatusProblem.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusProblem
                    )
                )

                jobStatusInactive.setBackgroundResource(R.drawable.bg_status_inactive_stroke)
                jobStatusInactive.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusInactive
                    )
                )

                jobStatusComplete.setBackgroundResource(R.drawable.bg_status_complete_stroke)
                jobStatusComplete.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusComplete
                    )
                )
            }
            2 -> {
                jobStatusNew.setBackgroundResource(R.drawable.bg_status_new_stroke)
                jobStatusNew.setTextColor(ContextCompat.getColor(this, R.color.colorStatusNew))

                jobStatusProgress.setBackgroundResource(R.drawable.bg_status_progress_solid)
                jobStatusProgress.setTextColor(ContextCompat.getColor(this, R.color.colorFontWhite))

                jobStatusProblem.setBackgroundResource(R.drawable.bg_status_problem_stroke)
                jobStatusProblem.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusProblem
                    )
                )

                jobStatusInactive.setBackgroundResource(R.drawable.bg_status_inactive_stroke)
                jobStatusInactive.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusInactive
                    )
                )

                jobStatusComplete.setBackgroundResource(R.drawable.bg_status_complete_stroke)
                jobStatusComplete.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusComplete
                    )
                )
            }
            3 -> {
                jobStatusNew.setBackgroundResource(R.drawable.bg_status_new_stroke)
                jobStatusNew.setTextColor(ContextCompat.getColor(this, R.color.colorStatusNew))

                jobStatusProgress.setBackgroundResource(R.drawable.bg_progress_stroke)
                jobStatusProgress.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusProgress
                    )
                )

                jobStatusProblem.setBackgroundResource(R.drawable.bg_status_problem_solid)
                jobStatusProblem.setTextColor(ContextCompat.getColor(this, R.color.colorFontWhite))

                jobStatusInactive.setBackgroundResource(R.drawable.bg_status_inactive_stroke)
                jobStatusInactive.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusInactive
                    )
                )

                jobStatusComplete.setBackgroundResource(R.drawable.bg_status_complete_stroke)
                jobStatusComplete.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusComplete
                    )
                )
            }
            5 -> {
                jobStatusNew.setBackgroundResource(R.drawable.bg_status_new_stroke)
                jobStatusNew.setTextColor(ContextCompat.getColor(this, R.color.colorStatusNew))

                jobStatusProgress.setBackgroundResource(R.drawable.bg_status_progress_stroke)
                jobStatusProgress.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusProgress
                    )
                )

                jobStatusProblem.setBackgroundResource(R.drawable.bg_status_problem_stroke)
                jobStatusProblem.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusProblem
                    )
                )

                jobStatusInactive.setBackgroundResource(R.drawable.bg_status_inactive_solid)
                jobStatusInactive.setTextColor(ContextCompat.getColor(this, R.color.colorFontWhite))

                jobStatusComplete.setBackgroundResource(R.drawable.bg_status_complete_stroke)
                jobStatusComplete.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusComplete
                    )
                )

            }
            4 -> {
//                assignmentStatusLayout.visibility = View.GONE


                jobStatusNew.setBackgroundResource(R.drawable.bg_status_new_stroke)
                jobStatusNew.setTextColor(ContextCompat.getColor(this, R.color.colorStatusNew))

                jobStatusProgress.setBackgroundResource(R.drawable.bg_status_progress_stroke)
                jobStatusProgress.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusProgress
                    )
                )

                jobStatusProblem.setBackgroundResource(R.drawable.bg_status_problem_stroke)
                jobStatusProblem.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusProblem
                    )
                )

                jobStatusInactive.setBackgroundResource(R.drawable.bg_status_inactive_stroke)
                jobStatusInactive.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorStatusInactive
                    )
                )

                jobStatusComplete.setBackgroundResource(R.drawable.bg_status_complete_solid)
                jobStatusComplete.setTextColor(ContextCompat.getColor(this, R.color.colorFontWhite))

            }
        }


        // status update
        jobStatusNew.setOnClickListener {
            requestUpdateStatusApi(1)
        }
        jobStatusProgress.setOnClickListener {
            requestUpdateStatusApi(2)
        }
        jobStatusProblem.setOnClickListener {
            requestUpdateStatusApi(3)
        }
        jobStatusInactive.setOnClickListener {
            requestUpdateStatusApi(5)
        }
        jobStatusComplete.setOnClickListener {
            requestUpdateStatusApi(4)
        }
    }

    private fun requestUpdateStatusApi(status: Int) {
        val map = HashMap<String, Any>()
        map["jobid"] = jobId.toString()
        map["statusid"] = status.toString()
        ApiManager.getInstance().requestApi(ApiMode.UPDATE_JOB_STATUS, map, true, this, "POST")
    }

    private fun setupAttachments(attachments: ArrayList<AttachmentModel>) {
        if (attachments.isEmpty())
            return

        val attachmentAdapter = JobAttachmentAdapter(Glide.with(this), this)
        jobAttachmentsList.apply {
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
            adapter = attachmentAdapter
        }
        attachmentAdapter.addAll(attachments)
    }

    override fun onRecyclerItemClicked(pos: Int, view: View, data: AttachmentModel) {


        if (AppUtils.isImageUrl(data.url)) {
            launchActivity<FullScreenImageActivity> {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                putExtra("image", data.url)
            }
        } else
            AppUtils.openBrowser(data.url)
    }


    private fun setupClickListeners() {
        textView3.setOnClickListener {
            // full description
            requestJobDescriptionApi()
        }
        textView9.setOnClickListener {
            launchActivity<AllCommentsActivity>(201) {
                putExtra("jobid", jobId)
            }
        }
        viewContact.setOnClickListener {
            requestJobContactApi()

        }
        textView5.setOnClickListener {
            launchActivity<AddInventoryActivity>(201) {
                putExtra("jobid", jobId)
            }
        }

        setupAddCommentUI()
    }

    private fun setupAddCommentUI() {
        etJobComment.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                requestAddCommentApi(ApiMode.ADD_COMMENT, null)
            }
            true
        }

        etCommentImage.setOnClickListener {
            fileOrImage = 2
            requestFilePicker()
        }
    }

    private fun requestAddCommentApi(mode: ApiMode, imagePath: String?, fromCamera: String = "0") {
        if (TextUtils.isEmpty(imagePath) && etJobComment.text.toString().isEmpty()) {
            AppUtils.shortToast("Enter a valid comment")
        } else {
            etJobComment.showKeyboard(false)
            /* jobid,  body, userid,file*/


            /* HANDLING IMAGE ROTATION ON SOME DEVICES*/
//            val uri=Uri.fromFile(File(imagePath))
//            try {
//                val exif = ExifInterface(imagePath)
//                val orientation =
//                    exif.getAttributeInt(
//                        ExifInterface.TAG_ORIENTATION,
//                        ExifInterface.ORIENTATION_UNDEFINED
//                    )
//                val orientation=getOrientation(uri)
//                val bitmap =
//                    MediaStore.Images.Media.getBitmap(contentResolver, uri)
//                val rotatedBitmap = rotateBitmap(bitmap, orientation)
//
//                if (rotatedBitmap != null && bitmap != rotatedBitmap) {
//                    saveBitmapToFile(
//                        rotatedBitmap, uri
//                    )
//                }
//            } catch (e: IOException) {
//                Crashlytics.log(e.localizedMessage)
//            }

            val progressDialog = ProgressDialog(this)
            progressDialog.setCancelable(false)
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.setMessage("Uploading..")
            progressDialog.show()

            val builder = AndroidNetworking.upload(ApiManager.BASE_URL + mode.getName())
                .addMultipartParameter("jobid", jobId.toString())
                .addMultipartParameter("userid", AppPreference.getInstance().userId)
                .addMultipartParameter("camera", fromCamera)

            if (TextUtils.isEmpty(imagePath))
                builder.addMultipartParameter("body", etJobComment.text.toString())


            if (imagePath != null)
                builder.addMultipartFile("file", File(imagePath))


            builder.build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject?) {
                        progressDialog.dismiss()

                        if (response?.isNull("error")!! || TextUtils.isEmpty(response.getString("error"))) {
                            etJobComment.setText("")
                            AppUtils.shortToast("Comment added")
                            requestJobDetailApi()
                        } else {
                            AppUtils.shortToast(response.getString("error"))
                        }
                    }

                    override fun onError(anError: ANError?) {
                        progressDialog.dismiss()
                        anError?.printStackTrace()
                        AppUtils.showException()
                    }
                })
        }
    }


    private fun setupJobDescriptionDialog(data: String?) {
        val dialog = Dialog(this, R.style.Theme_Dialog)
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_description, null)
        dialog.setContentView(view)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        view.dialogClose.setOnClickListener {
            dialog.dismiss()
        }

        // set data to view
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            view.jobDetailDescription.text = Html.fromHtml(data, Html.FROM_HTML_MODE_LEGACY)
        } else
            view.jobDetailDescription.text = Html.fromHtml(data)

        dialog.show()

    }

    private fun setupJobContactDialog(data: JsonObject?) {
        val dialog = Dialog(this, R.style.Theme_Dialog)
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_view_contact, null)
        dialog.setContentView(view)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        view.dialogClose.setOnClickListener {
            dialog.dismiss()
        }

        view.btnMakeCall
            .setOnClickListener {
                AppUtils.openDialer(view.contactTelephone.text.toString())
            }
        view.btnSendEmail
            .setOnClickListener {
                AppUtils.openGmail("", view.contactEmail.text.toString())
            }

        // set data to view
        view.contactCompanyName.text = data?.get("companyname")?.asString
        view.contactPrimaryName.text = data?.get("customername")?.asString
        view.contactTelephone.text = data?.get("customerphone")?.asString
        view.contactEmail.text = data?.get("customeremail")?.asString


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            view.contactJobLocation.text =
                Html.fromHtml(data?.get("jobaddress")?.asString, Html.FROM_HTML_MODE_LEGACY)
        } else
            view.contactJobLocation.text = Html.fromHtml(data?.get("jobaddress")?.asString)

        dialog.show()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 201 && resultCode == Activity.RESULT_OK) {
            requestJobDetailApi()
        } else if (requestCode == FilePickerConst.REQUEST_CODE_DOC && resultCode == Activity.RESULT_OK && data != null) {

            try {
                val fileName = FileUtils().getFileName(this, data.data)
                val file =
                    File.createTempFile(
                        "crewtok",
                        fileName.substring(fileName.lastIndexOf("."), fileName.length)
                    )

                val inputStream = contentResolver.openInputStream(data.data!!)

                val maxbufferSixe = 1 * 1024 * 1024

                val bytesAvailable = inputStream?.available()
                val bufferSize = min(bytesAvailable!!, maxbufferSixe)
                val buffers = ByteArray(bufferSize)

                val outputStream = FileOutputStream(file)
                var read = 0

                while (inputStream.read(buffers).also {
                        read = it
                    } != -1) {
                    outputStream.write(buffers, 0, read)

                }
                inputStream.close()
                outputStream.close()

                requestUploadFileApi(
                    file.absolutePath,
                    ApiMode.ADD_ATTACHMENT
                )
            } catch (ex: Exception) {
                ex.printStackTrace()
                AppUtils.showException()
            }
        } else if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO && resultCode == Activity.RESULT_OK
            && data != null
        ) {
            requestAddCommentApi(
                ApiMode.ADD_COMMENT,
                data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA)[0], "0"
            )
        } else if (requestCode == REQUEST_IMAGE_CAPTURE
            && resultCode == Activity.RESULT_OK && data != null
        ) {
            val filePath = data.getStringExtra("filePath")
            requestUploadFileApi(
                filePath,
                ApiMode.ADD_ATTACHMENT
            )
        }
    }


//    fun File.copyInputStreamToFile(inputStream: InputStream) {
//        inputStream.use { input ->
//            this.outputStream().use { fileOut ->
//                input.copyTo(fileOut)
//            }
//        }
//    }

    private fun setupAddAttachment() {
        btnAddAttachment.setOnClickListener {
            //            fileOrImage = 1
            showMediaSelectionChooserDialog()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 301 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            requestFilePicker() //1=file, 2=image
        }
    }

    private fun showMediaSelectionChooserDialog() {

        val dialog = AlertDialog.Builder(this)
            .setTitle("Pick Image")
            .setMessage("Choose a source to select file")
            .setPositiveButton("File Storage") { d, _ ->
                d.dismiss()
                requestFilePicker()
            }
            .setNegativeButton("Camera") { d, _ ->
                d.dismiss()
                launchCameraIntent()
            }
            .setNeutralButton("Dismiss") { d, _ ->
                d.dismiss()
            }
            .create()
        dialog.show()
        dialog.getButton(Dialog.BUTTON_POSITIVE)
            .setBackgroundColor(resources.getColor(android.R.color.transparent))
        dialog.getButton(Dialog.BUTTON_POSITIVE)
            .setTextColor(resources.getColor(R.color.colorPrimaryDark))
        dialog.getButton(Dialog.BUTTON_NEGATIVE)
            .setBackgroundColor(resources.getColor(android.R.color.transparent))
        dialog.getButton(Dialog.BUTTON_NEGATIVE)
            .setTextColor(resources.getColor(R.color.colorPrimaryDark))
        dialog.getButton(Dialog.BUTTON_NEUTRAL)
            .setBackgroundColor(resources.getColor(android.R.color.transparent))
        dialog.getButton(Dialog.BUTTON_NEUTRAL)
            .setTextColor(resources.getColor(R.color.colorPrimaryDark))


    }

    private fun requestFilePicker(/*fileOrImage: Int*/) {

        if (ActivityCompat.checkSelfPermission(
                this,
                "android.permission.WRITE_EXTERNAL_STORAGE"
            ) == PERMISSION_GRANTED
        ) {
//            if (fileOrImage == 1) {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "*/*"
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(intent, FilePickerConst.REQUEST_CODE_DOC)
//            } else
//                showMediaSelectionChooserDialog()
        } else {
            ActivityCompat.requestPermissions(
                this@AssignmentDetail,
                arrayOf("android.permission.WRITE_EXTERNAL_STORAGE"), 301
            )
        }
    }

    private fun requestUploadFileApi(url: String, mode: ApiMode) {
        val progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.setMessage("Uploading..")
        progressDialog.show()
        AndroidNetworking.upload(ApiManager.BASE_URL + mode.getName())
            .addMultipartParameter("Jobid", jobId.toString())
            .addMultipartFile("file", File(url))
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    if (progressDialog.isShowing)
                        progressDialog.dismiss()
                    if (response?.isNull("error")!! || TextUtils.isEmpty(response.getString("error"))) {
                        AppUtils.shortToast("Attachment uploaded successfully")
                        requestJobDetailApi()
                    }
                }

                override fun onError(anError: ANError?) {
                    progressDialog.dismiss()
                    anError?.printStackTrace()
                    AppUtils.showException()
                }
            })
    }


}