package com.crewtok.ui.assignmentdetail

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.crewtok.R
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.core.MyApplication
import com.crewtok.data.AttachmentModel
import kotlinx.android.synthetic.main.item_job_attachment.view.*

class JobAttachmentAdapter(val requestManager: RequestManager, val callback: OnRecyclerViewItemClick<AttachmentModel>) :
    RecyclerView.Adapter<JobAttachmentAdapter.JobAttachmentVh>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(MyApplication.instance.getContext())

    val list = ArrayList<AttachmentModel>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobAttachmentVh {
        return JobAttachmentVh(layoutInflater.inflate(R.layout.item_job_attachment, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun addAll(data: ArrayList<AttachmentModel>) {
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: JobAttachmentVh, position: Int) {
        holder.bind(list[holder.adapterPosition])
    }

    inner class JobAttachmentVh(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: AttachmentModel) {


//            try {
            requestManager.load(model.url)
                .apply(
                    RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.ic_file)
                        .centerCrop()
                )
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        modell: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {

//                        model.type = "file"
//                        itemView.itemJobAttachmentImage.setImageResource(R.drawable.ic_file)
                        e?.printStackTrace()
                        return false

                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        itemView.itemJobAttachmentImage.setImageDrawable(resource)

                        return true

                    }
                })
                .into(itemView.itemJobAttachmentImage)
//            } catch (e: Exception) {
//
//
//            }


            itemView.setOnClickListener {
                callback.onRecyclerItemClicked(adapterPosition, itemView, model)
            }
        }
    }
}