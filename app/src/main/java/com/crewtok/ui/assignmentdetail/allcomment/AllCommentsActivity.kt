package com.crewtok.ui.assignmentdetail.allcomment

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import androidx.recyclerview.widget.DividerItemDecoration
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bumptech.glide.Glide
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.data.ItemCommentModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.ui.fullimage.FullScreenImageActivity
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.crewtok.util.launchActivity
import com.crewtok.util.showKeyboard
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import droidninja.filepicker.FilePickerConst
import kotlinx.android.synthetic.main.activity_all_comments.*
import org.json.JSONObject
import java.io.File

class AllCommentsActivity : BaseDrawerActivity(), ApiResponse,
    OnRecyclerViewItemClick<ItemCommentModel> {


    var jobId: Int = -1

    private lateinit var commentsAdapter: AllCommentsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_comments)

        getParameters()

        setupCommentsList().also {
            requestAllCommentsApi()
        }

        setupAddCommentUI()

        allCommentDialogClose.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {
        val typeToken = object : TypeToken<ArrayList<ItemCommentModel>>() {}.type
        val list = Gson().fromJson<ArrayList<ItemCommentModel>>(
            jsonObject?.getAsJsonArray("data"),
            typeToken
        )
        if (list.isEmpty()) AppUtils.shortToast("No Comments")
        else
            updateCommentsAdapter(list)

    }

    private fun updateCommentsAdapter(list: ArrayList<ItemCommentModel>) {
        commentsAdapter.addAllComments(list)
        allCommentRecycler.smoothScrollToPosition(commentsAdapter.itemCount - 1)
    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        e?.printStackTrace()
        AppUtils.showException()
    }


    private fun getParameters() {
        if (intent?.hasExtra("jobid")!!) {
            jobId = intent?.getIntExtra("jobid", -1)!!
        }
    }


    private fun setupCommentsList() {
        commentsAdapter = AllCommentsAdapter(Glide.with(this), this)
        allCommentRecycler.apply {
            adapter = commentsAdapter
            addItemDecoration(DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL))
        }
    }

    private fun requestAllCommentsApi() {
        val map = HashMap<String, Any>()
        map["jobId"] = jobId

        ApiManager.getInstance().requestApi(ApiMode.COMMENT_LIST, map, true, this, "GET")
    }

    private fun setupAddCommentUI() {
        etAllCommentAdd.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                requestAddCommentApi(ApiMode.ADD_COMMENT, null)
            }
            true
        }

        etAllCommentAddImage.setOnClickListener {
            requestFilePicker()
        }
    }

    private fun requestFilePicker() {

        if (ActivityCompat.checkSelfPermission(
                this,
                "android.permission.WRITE_EXTERNAL_STORAGE"
            ) == PermissionChecker.PERMISSION_GRANTED
        ) {
            showMediaSelectionChooser()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf("android.permission.WRITE_EXTERNAL_STORAGE"), 301
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 301 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            requestFilePicker()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO && resultCode == Activity.RESULT_OK
//            && data != null
//        ) {
//
//            val path = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA)[0]
//            requestAddCommentApi(
//                ApiMode.ADD_COMMENT,
//                path, "0"
//
//            )
//        } else
//            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK && data != null) {
//                val filePath = data.getStringExtra("filePath")
//                requestAddCommentApi(
//                    ApiMode.ADD_COMMENT,
//                    filePath, "1"
//                )
//            }
    }

    private fun requestAddCommentApi(mode: ApiMode, filePath: String?, fromCamera: String = "0") {
        if (etAllCommentAdd.text.toString().isEmpty()) {
            AppUtils.shortToast("Enter a valid comment")
        } else {
            etAllCommentAdd.showKeyboard(false)
            /* jobid,  body, userid,file*/


//            try {
//            val exif = ExifInterface(filePath)
//                val orientation =
//                    exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
//                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, Uri.parse(filePath))
//                val rotatedBitmap = rotateBitmap(bitmap, orientation)
//
//                if (rotatedBitmap != null && bitmap != rotatedBitmap) {
//                    saveBitmapToFile(rotatedBitmap, Uri.parse(filePath))
//                }
//            } catch (e: IOException) {
//                Crashlytics.log(e.localizedMessage)
//            }


            val progressDialog = ProgressDialog(this)
            progressDialog.setCancelable(false)
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.setMessage("Uploading..")
            progressDialog.show()
            val builder = AndroidNetworking.upload(ApiManager.BASE_URL + mode.getName())
                .addMultipartParameter("jobid", jobId.toString())
                .addMultipartParameter("userid", AppPreference.getInstance().userId)
//                .addMultipartParameter("exifInfo", Gson().toJson(exif))
//                .addMultipartParameter("fromCamera", fromCamera)


            if (TextUtils.isEmpty(filePath))
                builder.addMultipartParameter("body", etAllCommentAdd.text.toString())


            if (!TextUtils.isEmpty(filePath))
                builder.addMultipartFile("file", File(filePath))

            builder.build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject?) {
                        progressDialog.dismiss()
                        if (response?.isNull("error")!! || TextUtils.isEmpty(response.getString("error"))) {
                            etAllCommentAdd.setText("")
                            AppUtils.shortToast("Comment added")
                            val data = Gson().fromJson<ItemCommentModel>(
                                response.getJSONObject("data").toString(),
                                ItemCommentModel::class.java
                            )
                            commentsAdapter.addComment(data)
                            allCommentRecycler.scrollToPosition(commentsAdapter.itemCount - 1)
                        } else {
                            AppUtils.shortToast(response.getString("error"))
                        }
                    }

                    override fun onError(anError: ANError?) {
                        progressDialog.dismiss()
                        anError?.printStackTrace()
                        AppUtils.showException()
                    }
                })
        }
    }

    override fun onRecyclerItemClicked(pos: Int, view: View, data: ItemCommentModel) {
        launchActivity<FullScreenImageActivity> {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            putExtra("image", data.commentimage)
        }
    }
}
