package com.crewtok.ui.assignmentdetail.allcomment

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.crewtok.R
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.core.MyApplication
import com.crewtok.data.ItemCommentModel
import com.crewtok.util.AppUtils
import kotlinx.android.synthetic.main.item_comment.view.*

class AllCommentsAdapter(
    val requestManager: RequestManager,
    val callback: OnRecyclerViewItemClick<ItemCommentModel>
) :
    RecyclerView.Adapter<AllCommentsAdapter.ItemCommentVh>() {

    val list = ArrayList<ItemCommentModel>()
    private val layoutInflater: LayoutInflater =
        LayoutInflater.from(MyApplication.instance.getContext())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCommentVh {
        return ItemCommentVh(layoutInflater.inflate(R.layout.item_comment, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemCommentVh, position: Int) {
        holder.bind(list[holder.adapterPosition])
    }

    fun addAllComments(data: ArrayList<ItemCommentModel>) {
        list.addAll(data)
        notifyDataSetChanged()
    }

    fun addComment(data: ItemCommentModel) {
        list.add(data)
        notifyItemInserted(list.size)
    }

    override fun onViewRecycled(holder: ItemCommentVh) {
        requestManager.clear(holder.itemView.itemCommentUserImage)

//        if (!TextUtils.isEmpty(list[holder.adapterPosition].commentimage))
//            requestManager.clear(holder.itemView.itemCommentImage)
    }


    inner class ItemCommentVh(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: ItemCommentModel) {
            itemView.itemCommentName.text = model.name
            itemView.itemCommentBody.text = model.body
            itemView.itemCommentDate.text = model.craeteddate

            if (TextUtils.isEmpty(model.image))
                return

            requestManager.load(AppUtils.getFullImageUrl(model.image!!))
                .apply(RequestOptions.placeholderOf(R.drawable.ic_user))
                .into(itemView.itemCommentUserImage)

//            if (!TextUtils.isEmpty(model.commentimage)) {
//                itemView.itemCommentImage.visibility = View.VISIBLE
//                requestManager.load((model.commentimage))
//                    .apply(
//                        RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)
//                            .encodeQuality(40)
//                            .placeholder(R.drawable.image_placeholder)
//                    )
//                    .into(itemView.itemCommentImage)
//
//                itemView.itemCommentImage.setOnClickListener {
//                    callback.onRecyclerItemClicked(adapterPosition, it, model)
//                }
//
//            }

        }
    }
}