package com.crewtok.ui.inventory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.crewtok.data.InventoryCategoryModel

class InventoryCategoryAdapter(val list: ArrayList<InventoryCategoryModel>) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v = convertView
        if (v == null)
            v = LayoutInflater.from(parent?.context)
                .inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)


        // set data to item
        (v!! as TextView).text = list[position].Tagname

        return v
    }

    override fun getItem(position: Int): Any {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return list[position].tagid.toLong()
    }

    override fun getCount(): Int {
        return list.size
    }

}