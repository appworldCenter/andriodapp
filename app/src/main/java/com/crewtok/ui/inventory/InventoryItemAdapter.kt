package com.crewtok.ui.inventory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.crewtok.data.InventoryItemModel

class InventoryItemAdapter(val list: ArrayList<InventoryItemModel>) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var v = convertView
        if (v == null)
            v = LayoutInflater.from(parent?.context)
                .inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)


        // set data to item
        (v!! as TextView).text = list[position].name

        return v
    }

    override fun getItem(position: Int): InventoryItemModel {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return list[position].id.toLong()
    }

    override fun getCount(): Int {
        return list.size
    }

}