package com.crewtok.ui.inventory

import android.app.Activity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.AdapterView
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.data.InventoryCategoryModel
import com.crewtok.data.InventoryItemModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_add_inventory.*

class AddInventoryActivity : BaseDrawerActivity(), ApiResponse, AdapterView.OnItemSelectedListener {


    private var groupId: Long = -1
    private var jobId = -1
    private var inventoryId: Long = -1
    private var selectedinventoryQuantity: Int = 0
    lateinit var categoryAdapter: InventoryCategoryAdapter
    lateinit var itemAdapter: InventoryItemAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_inventory)
        getParameters()
        requestInventoryCategoriesApi()
        setupUiWidgets()
    }

    private fun getParameters() {
        if (intent?.hasExtra("jobid")!!) {
            jobId = intent.getIntExtra("jobid", -1)
        } else finish()
    }

    private fun setupUiWidgets() {
        inventoryDialogClose.setOnClickListener {
            finish()
        }

        btnSaveInventory.setOnClickListener {
            validateQuantityAndSave()
        }
    }


    private fun requestInventoryCategoriesApi() {
        val map = HashMap<String, Any>()
        map["userId"] = AppPreference.getInstance().userId

        ApiManager.getInstance().requestApi(ApiMode.INVENTORY_CATEGORY, map, true, this, "GET")
    }


    /*userId,groupid*/
    private fun requestInventoryItemsApi() {
        val map = HashMap<String, Any>()
        map["userId"] = AppPreference.getInstance().userId
        map["groupid"] = groupId
        ApiManager.getInstance().requestApi(ApiMode.INVENTORY_ITEMS, map, true, this, "GET")

    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {

        when (mode) {
            ApiMode.INVENTORY_CATEGORY -> {
                val typeToken = object : TypeToken<ArrayList<InventoryCategoryModel>>() {}.type
                val list = Gson().fromJson<ArrayList<InventoryCategoryModel>>(
                    jsonObject?.getAsJsonArray("data"),
                    typeToken
                )
                if (list.isNullOrEmpty())
                    return
                setupCategorySpinner(list)

            }
            ApiMode.INVENTORY_ITEMS -> {
                val typeToken = object : TypeToken<ArrayList<InventoryItemModel>>() {}.type
                val list = Gson().fromJson<ArrayList<InventoryItemModel>>(
                    jsonObject?.getAsJsonArray("data"),
                    typeToken
                )
                if (list.isNullOrEmpty())
                    return
                setupItemSpinner(list)

            }
            ApiMode.ADD_INVENTORY -> {
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        e?.printStackTrace()
        AppUtils.showException()
    }

    private fun setupCategorySpinner(list: ArrayList<InventoryCategoryModel>) {
        categoryAdapter = InventoryCategoryAdapter(list)
        inventoryCategory.adapter = categoryAdapter
        inventoryCategory.onItemSelectedListener = this
    }

    private fun setupItemSpinner(list: ArrayList<InventoryItemModel>) {
        itemAdapter = InventoryItemAdapter(list)
        inventoryList.adapter = itemAdapter
        inventoryList.onItemSelectedListener = this
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id) {
            inventoryCategory.id -> {
                groupId = id
                requestInventoryItemsApi()
            }
            inventoryList.id -> {
                inventoryId = id
                selectedinventoryQuantity = itemAdapter.getItem(position).Quantity
            }
        }
    }


    private fun validateQuantityAndSave() {
        if (TextUtils.isEmpty(inventoryQuantity.text.toString()) || inventoryQuantity.text.toString().trim() == "0")
            AppUtils.shortToast("Enter a valid quantity")
        else if (inventoryQuantity.text.toString().toInt() > selectedinventoryQuantity) {
            AppUtils.shortToast("Total available quantity is $selectedinventoryQuantity")
        } else {
            requestAddInventoryApi()
        }

    }


    /* id, jobid, Quantity*/
    private fun requestAddInventoryApi() {
        val map = HashMap<String, Any>()
        map["Quantity"] = inventoryQuantity.text.toString()
        map["jobid"] = jobId
        map["id"] = inventoryId
        ApiManager.getInstance().requestApi(ApiMode.ADD_INVENTORY, map, true, this, "POST")
    }
}
