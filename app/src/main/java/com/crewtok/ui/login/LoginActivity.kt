package com.crewtok.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.crewtok.R
import com.crewtok.base.BaseActivity
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.ui.dashboard.DashboardActivity
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.crewtok.util.launchActivity
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_login.*
import java.lang.Exception

class LoginActivity : BaseActivity(), View.OnClickListener, ApiResponse {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        populateLastLoginDetails()

        btnSignIn.setOnClickListener(this)
        tvSupport.setOnClickListener(this)
        tvForgetPassword.setOnClickListener(this)
    }

    private fun populateLastLoginDetails() {
        loginEmail.setText(AppPreference.getInstance().userName)
        loginPassword.setText(AppPreference.getInstance().password)
    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.btnSignIn -> {
                validateLoginForm()
            }

            R.id.tvSupport -> {
                AppUtils.openBrowser("https://crewtok.com/contact.html")
//                AppUtils.openGmail("Crewtok support", "admin@crewtok.com")
            }
            R.id.tvForgetPassword -> {
                AppUtils.openBrowser("https://app.crewtok.com/account/forget-password")
//                AppUtils.openGmail("Crewtok support", "admin@crewtok.com")
            }
        }

    }

    private fun validateLoginForm() {
        if (!TextUtils.isEmpty(loginEmail.text) && !TextUtils.isEmpty(loginPassword.text)) {
            requestLoginApi()
        } else {
            AppUtils.shortToast("Email or password cannot be empty")
        }
    }

    private fun requestLoginApi() {
        val map = HashMap<String, Any>()
        map["email"] = loginEmail.text.toString()
        map["password"] = loginPassword.text.toString()
        map["device_id"] = AppUtils.getDeviceId()

        ApiManager.getInstance().requestApi(ApiMode.LOGIN, map, true, this, "POST")
    }


    /*{"data":{"Email":"umesh.kumar@appworldcenter.com",
    "image":"~/assets/images/icon/staff.png","name":"umesh kamboj","firstname":"umesh",
    "lastname":"kamboj"},"error":"","sessionid":"37"}*/
    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {
        AppPreference.getInstance().saveUserId(jsonObject?.get("sessionid")?.asString)
        AppPreference.getInstance().saveUserData(jsonObject?.getAsJsonObject("data")?.toString())
        AppPreference.getInstance().saveLoggedIn(true)
        saveLastLoggedIn()
        launchActivity<DashboardActivity> {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
        finish()
    }

    private fun saveLastLoggedIn() {
        AppPreference.getInstance().saveUserName(loginEmail.text.toString().trim())
        AppPreference.getInstance().savePassword(loginPassword.text.toString().trim())
    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        AppUtils.showException()
    }
}
