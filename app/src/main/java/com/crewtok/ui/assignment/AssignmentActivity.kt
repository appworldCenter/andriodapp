package com.crewtok.ui.assignment

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout.VERTICAL
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.crewtok.R
import com.crewtok.base.BaseDrawerActivity
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.data.AssignmentModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.ui.assignment.sort.SortAssignmentActivity
import com.crewtok.ui.assignmentdetail.AssignmentDetail
import com.crewtok.ui.dashboard.DashboardActivity
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.crewtok.util.launchActivity
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_assignment.*


class AssignmentActivity : BaseDrawerActivity(),
    OnRecyclerViewItemClick<AssignmentModel>, ApiResponse {


    lateinit var adapter: AssignmentAdapter

    lateinit var placeClient: PlacesClient

    companion object {
        lateinit var selectedAssignments: ArrayList<AssignmentModel>
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedAssignments = ArrayList()
        setCustomContentView(R.layout.activity_assignment)
        val header = findViewById<TextView>(R.id.toolbarTitle)
        header.text = "Assigned Jobs"
        initWidgets()
        setupAssignmentsList()
    }

    private fun initWidgets() {

//        Places.initialize(this, "")
//        placeClient = Places.createClient(this)


        btnPlanRoute.setOnClickListener {
            //            showLocationConfirmationDialog()
            if (selectedAssignments.isNotEmpty()) {
                val intent = Intent(this, SortAssignmentActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.putParcelableArrayListExtra("selectedAssignments", selectedAssignments)
                startActivity(intent)
            } else {
                AppUtils.shortToast("At-least one location must be selected")
            }
        }

        findViewById<ImageView>(R.id.imageView4).setOnClickListener {
            onBackPressed()

        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        adapter.clear()
        requestAssignmentListApi()
    }

    private fun showLocationConfirmationDialog() {
        val dialog = Dialog(this, R.style.Theme_Dialog)
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_location_confirmation, null)
        dialog.setContentView(view)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        view.findViewById<ImageView>(R.id.dialogClose)
            .setOnClickListener {
                dialog.dismiss()
            }
        dialog.show()
    }

    private fun setupAssignmentsList() {
        adapter = AssignmentAdapter(this)
        assignedJobList.layoutManager = LinearLayoutManager(this)
        assignedJobList.addItemDecoration(DividerItemDecoration(this, VERTICAL))
        assignedJobList.adapter = adapter

        requestAssignmentListApi()
    }

    private fun requestAssignmentListApi() {
        val map = HashMap<String, Any>()
        map["userid"] = AppPreference.getInstance().userId
        ApiManager.getInstance().requestApi(ApiMode.ASSIGNMENT_LIST, map, false, this, "POST")
    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {

        when (mode) {
            ApiMode.CLOCKIN -> {
                launchTimeTrackDialog(
                    jsonObject?.getAsJsonObject("data")?.get("isclockin")?.asBoolean!!,
                    jsonObject?.getAsJsonObject("data")?.get("lastmessage")?.asString
                )
            }
            ApiMode.ASSIGNMENT_LIST -> {
                progressBar.visibility = View.GONE
                val typeToken = object : TypeToken<List<AssignmentModel>>() {}.type
                val data = ArrayList(
                    Gson().fromJson<List<AssignmentModel>>(
                        jsonObject?.getAsJsonArray("data"),
                        typeToken
                    )
                )
                adapter.addAssignments(data)
            }
        }


    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.toString())
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        AppUtils.showException()
    }

    override fun onRecyclerItemClicked(pos: Int, view: View, data: AssignmentModel) {
        launchActivity<AssignmentDetail> {
            putExtra("assignment_id", data.jobId)
        }
    }

    override fun onBackPressed() {
        launchActivity<DashboardActivity> {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        finish()
    }

}
