package com.crewtok.ui.assignment.sort

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crewtok.R
import com.crewtok.callback.OnStartDragListener
import com.crewtok.callback.SimpleItemTouchHelperCallback
import com.crewtok.data.AssignmentModel
import com.crewtok.ui.locationselect.LocationSelectionActivity
import com.crewtok.util.launchActivity
import kotlinx.android.synthetic.main.activity_sort_assignment.*
import java.net.URLEncoder


class SortAssignmentActivity : AppCompatActivity(), OnStartDragListener {


    private lateinit var mItemTouchHelper: ItemTouchHelper
    private lateinit var assignments: ArrayList<AssignmentModel>
//    private lateinit var allItems: ArrayList<AssignmentModel>

    lateinit var adapter: ReorderableAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sort_assignment)

        getParameters()
        setupRecyclerView()

        reorderToolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        btnProceedToMap.setOnClickListener {
            launchActivity<LocationSelectionActivity>(1221) { }
        }
    }


    private fun getParameters() {
        assignments = intent?.getParcelableArrayListExtra("selectedAssignments")!!
    }

    private fun setupRecyclerView() {
        adapter = ReorderableAdapter(this)
        reorderableRecyclerView.layoutManager = LinearLayoutManager(this)
        reorderableRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        reorderableRecyclerView.adapter = adapter
        adapter.addAssignments(assignments)

        val callback = SimpleItemTouchHelperCallback(adapter)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper.attachToRecyclerView(reorderableRecyclerView)
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder?) {
        mItemTouchHelper.startDrag(viewHolder!!)
    }


    private fun launchMapNavigation(startAddress: String) {

        val allItems = adapter.getItems()


        var str = ""
        if (allItems.size > 1) {
            str = allItems.subList(0, allItems.size - 1).joinToString(
                "|", transform = {
                    it.location
                })
        }


        var ss =
            "https://www.google.com/maps/dir/?api=1&origin=" + startAddress + "&destination=" +
                    allItems[allItems.size - 1].location

        if (!TextUtils.isEmpty(str))
            ss += "&waypoints=" + URLEncoder.encode(str, "UTF-8")

        ss += "&travelmode=driving&dir_action=navigate"

        val gmmIntentUri = Uri.parse(ss)
        val intent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setPackage("com.google.android.apps.maps")
        try {
            startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            try {
                val unrestrictedIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                startActivity(unrestrictedIntent)
            } catch (innerEx: ActivityNotFoundException) {
                Toast.makeText(this, "Please install a maps application", Toast.LENGTH_LONG).show()
            }

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1221 && resultCode == Activity.RESULT_OK && data != null) {
            launchMapNavigation(data.getStringExtra("address"))
        }
    }
}
