package com.crewtok.ui.assignment.sort

import android.graphics.Color
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MotionEventCompat
import androidx.recyclerview.widget.RecyclerView
import com.crewtok.R
import com.crewtok.base.BaseViewHolder
import com.crewtok.callback.ItemTouchHelperAdapter
import com.crewtok.callback.ItemTouchHelperViewHolder
import com.crewtok.callback.OnStartDragListener
import com.crewtok.data.AssignmentModel
import kotlinx.android.synthetic.main.item_assigned_job_reorderable.view.*
import java.util.*
import kotlin.collections.ArrayList

class ReorderableAdapter(val dragListener: OnStartDragListener) :
    RecyclerView.Adapter<ReorderableAdapter.AssignmentVh>(), ItemTouchHelperAdapter {


    private val list = ArrayList<AssignmentModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssignmentVh {
        return AssignmentVh(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_assigned_job_reorderable,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: AssignmentVh, position: Int) {
        holder.bind(list[holder.adapterPosition])

        holder.itemView.setOnTouchListener { v, event ->
            if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                dragListener.onStartDrag(holder)
            }
            false
        }

    }

    fun getItems():List<AssignmentModel>{
        return list
    }

    fun addAssignments(a: ArrayList<AssignmentModel>) {
        a.forEach {
            add(it)
        }
    }

    fun add(model: AssignmentModel) {
        list.add(model)
        notifyItemInserted(list.size)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(list, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun onItemDismiss(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }


    inner class AssignmentVh(itemView: View) : BaseViewHolder(itemView), ItemTouchHelperViewHolder {


        override fun onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY)
        }

        override fun onItemClear() {
            itemView.setBackgroundColor(0)
        }


        lateinit var model: AssignmentModel
        fun bind(model: AssignmentModel) {
            this.model = model
            itemView.jobDate.text = model.assigndate
            itemView.jobName.text = model.jobName
            itemView.jobPO.text = "# " + model.po
            itemView.jobLocation.text = model.location

        }

        override fun onClick(v: View?) {
            // not used
        }
    }
}