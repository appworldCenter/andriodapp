package com.crewtok.ui.assignment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.crewtok.R
import com.crewtok.base.BaseViewHolder
import com.crewtok.callback.OnRecyclerViewItemClick
import com.crewtok.data.AssignmentModel
import kotlinx.android.synthetic.main.item_assigned_job.view.*

class AssignmentAdapter(val callback: OnRecyclerViewItemClick<AssignmentModel>?) :
    RecyclerView.Adapter<AssignmentAdapter.AssignmentVh>() {


    private val list = ArrayList<AssignmentModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssignmentVh {
        return AssignmentVh(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_assigned_job,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: AssignmentVh, position: Int) {
        holder.bind(list[holder.adapterPosition])
    }

    fun addAssignments(a: ArrayList<AssignmentModel>) {
        a.forEach {
            add(it)
        }
    }

    fun add(model: AssignmentModel) {
        list.add(model)
        notifyItemInserted(list.size)
    }

    fun clear()
    {
        list.clear()
        notifyDataSetChanged()
    }


    inner class AssignmentVh(itemView: View) : BaseViewHolder(itemView) {
        lateinit var model: AssignmentModel
        fun bind(model: AssignmentModel) {
            this.model = model
            itemView.jobDate.text = model.assigndate
            itemView.jobName.text = model.jobName
            itemView.jobPO.text = "# " + model.po
            itemView.jobLocation.text = model.location

            itemView.assignmentCheckbox.isChecked = AssignmentActivity.selectedAssignments.contains(model)

            itemView.jobName.setOnClickListener(this)
            itemView.imageView2.setOnClickListener(this)
            itemView.jobLocation.setOnClickListener(this)


            itemView.assignmentCheckbox.setOnCheckedChangeListener { buttonView, isChecked ->

                if (isChecked) {
//                    if (!AssignmentActivity.selectedAssignments.contains(model)) {
                    AssignmentActivity.selectedAssignments.add(model)
//                    }
                } else {
                    AssignmentActivity.selectedAssignments.remove(model)
                }
            }
        }

        override fun onClick(v: View?) {
            callback?.onRecyclerItemClicked(adapterPosition, v!!, model)
        }
    }
}