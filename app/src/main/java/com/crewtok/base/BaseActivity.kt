package com.crewtok.base

import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.crewtok.R
import com.crewtok.core.MyApplication

abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        MyApplication.instance.setContext(this)
    }

    fun setupBackPress() {
        findViewById<ImageView>(R.id.imageView4)
            .setOnClickListener {
                finish()
            }
    }


    override fun onResume() {
        super.onResume()
        MyApplication.instance.setContext(this)
    }
}