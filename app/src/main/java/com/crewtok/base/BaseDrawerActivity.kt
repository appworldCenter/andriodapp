package com.crewtok.base

import Router
import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.bumptech.glide.Glide
import com.crashlytics.android.Crashlytics
import com.crewtok.R
import com.crewtok.core.MyApplication
import com.crewtok.data.DashboardModel
import com.crewtok.network.ApiManager
import com.crewtok.network.ApiMode
import com.crewtok.network.ApiResponse
import com.crewtok.ui.assignment.AssignmentActivity
import com.crewtok.ui.camera.CustomCameraActivity
import com.crewtok.ui.communicate.CommunicateActivity
import com.crewtok.ui.dashboard.DashboardActivity
import com.crewtok.ui.equipment.EquipmentActivity
import com.crewtok.ui.login.LoginActivity
import com.crewtok.util.AppPreference
import com.crewtok.util.AppUtils
import com.crewtok.util.TbmCurrentLocation
import com.crewtok.util.launchActivity
import com.google.android.material.navigation.NavigationView
import com.google.gson.JsonObject
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import de.hdodenhof.circleimageview.CircleImageView
import droidninja.filepicker.FilePickerBuilder
import kotlinx.android.synthetic.main.activity_base_drawer.*
import kotlinx.android.synthetic.main.dialog_time_track.view.*
import java.io.IOException

abstract class BaseDrawerActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
    ApiResponse {


    protected var currentLocationName = ""
    protected lateinit var mDrawer: DrawerLayout
    private var contentLayout: FrameLayout? = null
    protected var navigationView: NavigationView? = null
    protected lateinit var hoursWorked: TextView
    protected lateinit var username: TextView
    protected lateinit var profileImage: CircleImageView
    protected lateinit var navHeaderView: View

    private lateinit var filePicker: FilePickerBuilder
    protected val REQUEST_IMAGE_CAPTURE = 9876

    protected var currentPhotoPath: String? = null


    companion object {
        var dashboardModel: DashboardModel? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_drawer)
        Crashlytics.setUserEmail(AppPreference.getInstance().userData.Email)
        mDrawer = findViewById(R.id.baseDrawer)
        contentLayout = findViewById(R.id.contentLayout)
        navigationView = findViewById(R.id.navigationView)
        navigationView!!.setNavigationItemSelectedListener(this)
        navHeaderView = navigationView!!.getHeaderView(0)
        hoursWorked = navHeaderView.findViewById(R.id.hoursWorked)
        username = navHeaderView.findViewById(R.id.nav_header_username)
        profileImage = navHeaderView.findViewById(R.id.profileImage)

        navDrawerIcon
            .setOnClickListener { mDrawer.openDrawer(GravityCompat.START) }

        txtLogout.setOnClickListener {
            Router.start<LoginActivity>(true)
            finish()
        }

        filePicker = FilePickerBuilder.instance.setMaxCount(1)

    }

    protected fun setCustomContentView(@LayoutRes layoutId: Int) {
        val inflater = this
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val contentView = inflater.inflate(layoutId, null, false)
        contentLayout!!.addView(contentView, 0)

        setupNavDrawer()
    }

    protected fun setupNavDrawer() {
        if (dashboardModel != null) {
            hoursWorked.text = "Hours: " + dashboardModel?.hour
            username.text = AppPreference.getInstance().userData.name

            Glide.with(this).load(dashboardModel?.profileimage)
                .placeholder(R.drawable.ic_user_white)
                .into(profileImage)

            Glide.with(this).load(dashboardModel?.profileimage)
                .placeholder(R.drawable.ic_user)
                .into(navDrawerIcon)


        }

    }


//    private fun launchTimeTrackDialog() {
//        val dialog = Dialog(this, R.style.Theme_Dialog)
//        val view = LayoutInflater.from(this).inflate(R.layout.dialog_time_track, null)
//        dialog.setContentView(view)
//        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        view.findViewById<ImageView>(R.id.dialogClose)
//            .setOnClickListener {
//                dialog.dismiss()
//            }
//        dialog.show()
//    }

    fun launchTimeTrackDialog(isClockedIn: Boolean, lastMessage: String?) {

        val dialog = Dialog(this, R.style.Theme_Dialog)
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_time_track, null)
        dialog.setContentView(view)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        view.time_tracking_username.text = AppPreference.getInstance().userData.name
        view.lastMessage.text = lastMessage
        view.time_tracking_date.text = Html.fromHtml("<b>Date: </b>" + AppUtils.getCurrentDate())
        view.time_tracking_time.text =
            Html.fromHtml("<b>Current time: </b>" + AppUtils.getCurrentTime())
        view.time_tracking_location.text = Html.fromHtml("<b>Location: </b>$currentLocationName")

        if (isClockedIn)
            view.btnClockIn.text = "Clock In"
        else
            view.btnClockIn.text = "Clock Out"

        view.dialogClose.setOnClickListener {
            dialog.dismiss()
        }
        view.btnClockIn.setOnClickListener {

            view.btnClockIn.visibility = View.GONE
            view.progressBar.visibility = View.VISIBLE
            val map = HashMap<String, Any>()
            map["userId"] = AppPreference.getInstance().userId
            map["location"] = currentLocationName
            map["time"] = AppUtils.getCurrentTime()

            ApiManager.getInstance().requestApi(
                ApiMode.UPDATE_CLOCKIN_STATUS, map, false,
                object : ApiResponse {
                    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {
                        AppUtils.shortToast("Updated successfully")
                        dialog.dismiss()
                    }

                    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
                        view.btnClockIn.visibility = View.VISIBLE
                        view.progressBar.visibility = View.GONE
                        AppUtils.shortToast(errorObject?.getAsJsonObject("data")?.get("error")?.asString)
                    }

                    override fun onException(e: java.lang.Exception?, mode: ApiMode?) {
                        view.btnClockIn.visibility = View.VISIBLE
                        view.progressBar.visibility = View.GONE
                        e?.printStackTrace()
                        AppUtils.showException()
                    }

                }, "POST"
            )
        }

        dialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        mDrawer.closeDrawer(GravityCompat.START)
        when (item.itemId) {

            R.id.nav_time_clock -> {
                handleTimeClock()
                return true
            }

            R.id.nav_assignment -> {
                Router.start<AssignmentActivity>()
                return true
            }
            R.id.nav_communicate -> {
                Router.start<CommunicateActivity>()
                return true
            }
            R.id.nav_equipment -> {
                Router.start<EquipmentActivity>()
                return true
            }
            R.id.nav_home -> {
                launchActivity<DashboardActivity> {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
                return true
            }

            R.id.nav_logout -> {

                MyApplication.instance.stopAndUnbind()
                AppPreference.getInstance().clearAllPreferences()
                launchActivity<LoginActivity> {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }
                finish()
                return true
            }
            else -> return false
        }
    }

    private fun handleTimeClock() {
        TbmCurrentLocation().fetchLocation(object : TbmCurrentLocation.CurrentLocationCallback {
            override fun currentLocationFetched(lat: Double, lng: Double) {
                val addresses = ArrayList<Address>()
                try {
                    val addr = Geocoder(this@BaseDrawerActivity).getFromLocation(lat, lng, 1)
                    addresses.addAll(addr)
                } catch (ex: IOException) {

                }
                if (addresses.isNotEmpty()) {
                    currentLocationName = addresses[0].getAddressLine(0)
                    requestClockInApi()
                } else {
                    AppUtils.shortToast("Unable to find a valid location")
                }
            }

            override fun currentLocationFetchFailed(error: String?) {
                AppUtils.shortToast("Cannot fetch location")
            }

        })
    }

    private fun requestClockInApi() {
        val map = HashMap<String, Any>()
        map["userId"] = AppPreference.getInstance().userId
        ApiManager.getInstance().requestApi(ApiMode.CLOCKIN, map, true, this, "POST")
    }

    override fun onSuccess(jsonObject: JsonObject?, mode: ApiMode?) {

        when (mode) {
            ApiMode.CLOCKIN -> {
                launchTimeTrackDialog(
                    jsonObject?.getAsJsonObject("data")?.get("isclockin")?.asBoolean!!,
                    jsonObject?.getAsJsonObject("data")?.get("lastmessage")?.asString
                )
            }
        }


    }

    override fun onFailure(errorObject: JsonObject?, mode: ApiMode?) {
        AppUtils.shortToast(errorObject?.get("error")?.asString)
    }

    override fun onException(e: Exception?, mode: ApiMode?) {
        e?.printStackTrace()
        AppUtils.showException()
    }

//    fun rotateBitmap(bitmap: Bitmap, orientation: Int): Bitmap? {
//        val matrix = Matrix()
//
//        when (orientation) {
//            ExifInterface.ORIENTATION_NORMAL ->
//                return bitmap
//            ExifInterface.ORIENTATION_FLIP_HORIZONTAL ->
//                matrix.setScale(-1f, 1f)
//
//            ExifInterface.ORIENTATION_ROTATE_180 ->
//                matrix.setRotate(180f)
//
//            ExifInterface.ORIENTATION_FLIP_VERTICAL -> {
//                matrix.setRotate(180f)
//                matrix.postScale(-1f, 1f)
//            }
//
//
//            ExifInterface.ORIENTATION_TRANSPOSE -> {
//                matrix.setRotate(90f)
//                matrix.postScale(-1f, 1f)
//            }
//
//            ExifInterface.ORIENTATION_ROTATE_90 ->
//                matrix.setRotate(90f)
//
//            ExifInterface.ORIENTATION_TRANSVERSE -> {
//                matrix.setRotate(-90f)
//                matrix.postScale(-1f, 1f)
//            }
//            ExifInterface.ORIENTATION_ROTATE_270 ->
//                matrix.setRotate(-90f)
//            else ->
//                return bitmap
//        }
//        try {
//            val bmRotated =
//                Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
//            bitmap.recycle()
//
//            return bmRotated
//        } catch (e: OutOfMemoryError) {
//            e.printStackTrace()
//            return null
//        }
//    }

//    fun saveBitmapToFile(croppedImage: Bitmap, saveUri: Uri?) {
//        if (saveUri != null) {
//            var outputStream: OutputStream? = null
//            try {
//                outputStream = contentResolver.openOutputStream(saveUri)
//                if (outputStream != null) {
//                    croppedImage.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)
//                }
//            } catch (e: IOException) {
//
//            } finally {
//                closeSilently(outputStream)
//                croppedImage.recycle()
//            }
//        }
//    }

//    protected fun getImageContentUri(imagePath: String): Uri {
//        val cursor = contentResolver.query(
//            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//            arrayOf(MediaStore.Images.Media._ID),
//            arrayOf(MediaStore.Images.Media.DATA + "=? ").toString(),
//            arrayOf(imagePath), null
//        )
//        if (cursor != null && cursor.moveToFirst()) {
//            val id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
//            cursor.close();
//            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
//        } else {
////    if (imagePath.exists()) {
//            val values = ContentValues()
//            values.put(MediaStore.Images.Media.DATA, imagePath)
//            return contentResolver.insert(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values
//            )!!
////    } else {
////      return null;
////    }
//        }
//    }

//    fun closeSilently(@Nullable c: Closeable?) {
//        if (c == null) {
//            return
//        }
//        try {
//            c.close()
//        } catch (t: Throwable) {
//            // Do nothing
//        }
//    }

    fun showMediaSelectionChooser() {

        val dialog = AlertDialog.Builder(this)
            .setTitle("Pick Image")
            .setMessage("Choose a source to select file")
            .setPositiveButton("File Storage") { d, _ ->
                d.dismiss()
                filePicker.withOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                    .enableCameraSupport(false)
                    .pickPhoto(this)
            }
            .setNegativeButton("Camera") { d, _ ->
                d.dismiss()
                launchCameraIntent()
            }
            .setNeutralButton("Dismiss") { d, _ ->
                d.dismiss()
            }
            .create()
        dialog.show()
        dialog.getButton(Dialog.BUTTON_POSITIVE)
            .setBackgroundColor(resources.getColor(android.R.color.transparent))
        dialog.getButton(Dialog.BUTTON_POSITIVE)
            .setTextColor(resources.getColor(R.color.colorPrimaryDark))
        dialog.getButton(Dialog.BUTTON_NEGATIVE)
            .setBackgroundColor(resources.getColor(android.R.color.transparent))
        dialog.getButton(Dialog.BUTTON_NEGATIVE)
            .setTextColor(resources.getColor(R.color.colorPrimaryDark))
        dialog.getButton(Dialog.BUTTON_NEUTRAL)
            .setBackgroundColor(resources.getColor(android.R.color.transparent))
        dialog.getButton(Dialog.BUTTON_NEUTRAL)
            .setTextColor(resources.getColor(R.color.colorPrimaryDark))


    }

    protected fun launchCameraIntent() {

        Dexter.withActivity(this).withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {

                    val takePictureIntent =
                        Intent(this@BaseDrawerActivity, CustomCameraActivity::class.java)
//                    Intent(this@BaseDrawerActivity,CustomCameraActivity::class.java)
//                        .also { takePictureIntent ->
//                            val photoFile: File? = try {
//                                createImageFile()
//                            } catch (ex: IOException) {
//                                AppUtils.shortToast("Some error occurred, Please try again!")
//                                null
//                            }
//
//                            photoFile?.also {
////                                val photoURI: Uri = FileProvider.getUriForFile(
////                                    this@BaseDrawerActivity,
////                                    "com.crewtok.fileprovider",
////                                    it
////                                )
//
//                                takePictureIntent.putExtra("filePath",currentPhotoPath)
////                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
//                                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
//                            }
//                    }
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }


                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    AppUtils.shortToast("Camera Permission Denied")
                }
            }).check()

    }

//    @Throws(IOException::class)
//    protected fun createImageFile(): File {
//        // Create an image file name
//        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
//        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
//        return File.createTempFile(
//            "JPEG_${timeStamp}_", /* prefix */
//            ".jpg", /* suffix */
//            storageDir /* directory */
//        ).apply {
//            // Save a file: path for use with ACTION_VIEW intents
//            currentPhotoPath = absolutePath
//        }
//    }
//
//    protected fun getOrientation(photoUri: Uri): Int {
//        var cursor = contentResolver.query(
//            photoUri,
//            arrayOf(MediaStore.Images.ImageColumns.ORIENTATION),
//            null, null, null
//        )
//
//        if (cursor.count != 1) {
//            cursor.close()
//            return -1
//        }
//
//        cursor.moveToFirst()
//        val orientation = cursor.getInt(0)
//        cursor.close()
//        return orientation
//    }


}
